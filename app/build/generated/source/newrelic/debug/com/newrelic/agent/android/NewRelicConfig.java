package com.newrelic.agent.android;
final class NewRelicConfig {
	static final String VERSION = "5.17.2";
	static final String BUILD_ID = "f79c72c0-2463-4257-bb37-ce9a801aeb2c";
	public static String getBuildId() {
		return BUILD_ID;
	}
}
