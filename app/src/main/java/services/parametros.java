package services;

import com.newrelic.com.google.gson.annotations.SerializedName;

public class  parametros {
    @SerializedName("url_firebase")
    private String url_firebase ="https://fcm.googleapis.com/v1/projects/rere-fbfbd/messages:send";
    @SerializedName("topic")
    private String topic;

    @SerializedName("parqueadero")
    private String parqueadero;

    @SerializedName("nickname")
    private String nickname;

    @SerializedName("conjunto")
    private String conjunto;

    @SerializedName("tipoparqueadero")
    private String tipoparqueadero;

    @SerializedName("visita")
    private String visita;


    public String getUrl_firebase() {
        return url_firebase;
    }


    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getParqueadero() {
        return parqueadero;
    }

    public void setParqueadero(String parqueadero) {
        this.parqueadero = parqueadero;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getConjunto() {
        return conjunto;
    }

    public void setConjunto(String conjunto) {
        this.conjunto = conjunto;
    }

    public String getTipoparqueadero() {
        return tipoparqueadero;
    }

    public void setTipoparqueadero(String tipoparqueadero) {
        this.tipoparqueadero = tipoparqueadero;
    }

    public String getVisita() {
        return visita;
    }

    public void setVisita(String visita) {
        this.visita = visita;
    }

    @Override
    public String toString() {
        return "parametros{" +
                "url_firebase='" + url_firebase + '\'' +
                ", topic='" + topic + '\'' +
                ", parqueadero='" + parqueadero + '\'' +
                ", nickname='" + nickname + '\'' +
                ", conjunto='" + conjunto + '\'' +
                ", tipoparqueadero='" + tipoparqueadero + '\'' +
                ", visita='" + visita +
                '}';
    }
}
