package services;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

public class LaunchNavigator
{
    public void startNavigation(double lat, double ln, Activity activity)
    {
        // Create a Uri from an intent string. Use the result to create an Intent.
        Uri gmmIntentUri = Uri.parse("google.navigation:q="+lat+","+ln);

// Create an Intent from gmmIntentUri. Set the action to ACTION_VIEW
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
// Make the Intent explicit by setting the Google Maps package
        mapIntent.setPackage("com.google.android.apps.maps");

// Attempt to start an activity that can handle the Intent
        activity.startActivity(mapIntent);
    }
}
