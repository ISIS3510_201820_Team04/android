package services;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface sendNotificationServices {

    @Headers({
            "content-type: application/json"
    })
    @POST("/certificacion/linnot/felipe/enviar")
    Call<ResponseSendNotification> sendNotify(@Body BodySenNotify body);

}
