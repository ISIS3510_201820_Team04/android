package services;

import com.newrelic.com.google.gson.annotations.SerializedName;

public class BodySenNotify {

    @SerializedName("titulo")
    private String titulo;
    @SerializedName("contenido")
    private String contenido;
    @SerializedName("destino")
    private String destino;
    @SerializedName("parametros")
    private parametros parametros;

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public services.parametros getParametros() {
        return parametros;
    }

    public void setParametros(services.parametros parametros) {
        this.parametros = parametros;
    }

    @Override
    public String toString() {
        return "BodySenNotify{" +
                "titulo='" + titulo + '\'' +
                ", contenido='" + contenido + '\'' +
                ", destino='" + destino + '\'' +
                ", parametros=" + parametros +
                '}';
    }
}
