package services;

import com.google.gson.annotations.SerializedName;

public class ResponseSendNotification {
    @SerializedName("name")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ResponseSendNotification{" +
                "name='" + name + '\'' +
                '}';
    }
}
