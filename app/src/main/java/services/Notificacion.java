package services;

import android.util.Log;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.support.constraint.Constraints.TAG;

public class Notificacion {

    public void post(BodySenNotify json) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://servicios.inube.com.co/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        sendNotificationServices api = retrofit.create(sendNotificationServices.class);
        retrofit2.Call<ResponseSendNotification> llamada =  api.sendNotify(json);

        llamada.enqueue(new retrofit2.Callback<ResponseSendNotification>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseSendNotification> call, Response<ResponseSendNotification> response) {
                Log.i(TAG, "SE REALIZO LA PETICION EXITOSAMENTE" + response.toString());
            }

            @Override
            public void onFailure(retrofit2.Call<ResponseSendNotification> call, Throwable t) {
                Log.i(TAG, "NO FUE POSIBLE REALIZAR LA PETICIÒN" + t.toString());
            }
        });

    }

    public BodySenNotify createObjNotify(String titulo, String contenido, String tokenDestino, parametros para){
        BodySenNotify body = new BodySenNotify();
        body.setTitulo(titulo);
        body.setContenido(contenido);
        body.setDestino(tokenDestino);
        body.setParametros(para);
        return  body;
    }

}
