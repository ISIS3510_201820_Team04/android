package services;

import android.os.AsyncTask;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Tareas extends AsyncTask<String, Void, String> {

    @Override
    protected String doInBackground(String... strings) {

        switch (strings[0]){

            case "LIBERAR_PARQUEADERO":
                liberarParqueadero(strings[1],strings[2], strings[3]);
                return null;
            case "NOTIFICACION":
                notificacion(strings[1],strings[2], strings[3]);
                return null;
            case "CONSULTAR_PARQUEADERO":
                try {
                    return  consultarParqueadero(strings[1],strings[2], strings[3]);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            default:
                return null;

        }
    }

    @Override
    protected void onPostExecute(String result) {



    }

    private void liberarParqueadero(String conjunto, String parqueadero, String tipoParquedero){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef2 = database.getReference("Conjuntos/"+ conjunto+ "/" + tipoParquedero + "/" + parqueadero);
        myRef2.child("Estado").setValue("Disponible");
    }

    String estado= "";
    private String consultarParqueadero(String conjunto, String parqueadero, String tipoParquedero) throws InterruptedException {
        FirebaseDatabase database = FirebaseDatabase.getInstance();

        final DatabaseReference myRef2 = database.getReference("Conjuntos/"+ conjunto+ "/" + tipoParquedero + "/" + parqueadero);
        myRef2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                estado = dataSnapshot.child("Estado").getValue().toString();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

        Thread.sleep(5000);
        return estado;

    }


    private void notificacion(String titulo, String contenido, String tokenDestino ){
        Notificacion nt = new Notificacion();
        parametros para = new parametros();
        nt.post(nt.createObjNotify(titulo,contenido,tokenDestino,para));
    }

}
