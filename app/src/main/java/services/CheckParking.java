package services;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;

import java.util.Timer;
import java.util.TimerTask;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class CheckParking extends IntentService {

    private static final String ACTION_PARKING_TIME = "services.action.PARKING_TIME";

    public CheckParking() {
        super("CheckParking");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_PARKING_TIME.equals(action)) {
                final String param1 = intent.getStringExtra("token");
                final String param2 = intent.getStringExtra("conjunto");
                final String param3 = intent.getStringExtra("parqueadero");
                final String param4 = intent.getStringExtra("tipoParqueadero");
                countTimeParking(param1, param2, param3, param4);
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionFoo(String param1, String param2) {
        // TODO: Handle action Foo
        throw new UnsupportedOperationException("Not yet implemented");
    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     */
    private void handleActionBaz(String param1, String param2) {
        // TODO: Handle action Baz
        throw new UnsupportedOperationException("Not yet implemented");
    }

    //long periodo = 300000;
    long periodo = 12000;
    int contador= 0;
    boolean bandera= true;
    private void countTimeParking(final String tk, final String conjunto, final String parqueadero, final String tipoParquedero){
        final Tareas task = new Tareas();
        final Timer t= new Timer();
        t.scheduleAtFixedRate(new TimerTask(){
            @Override
            public void run(){
                contador++;
                String estadoReserva = task.doInBackground("CONSULTAR_PARQUEADERO",conjunto,parqueadero,tipoParquedero);
                if(contador==3){
                    if(estadoReserva.equals("Reservado")){
                        task.doInBackground("NOTIFICACION","Atenciòn...", "Han pasado 15 minutos desde el momento de la reserva te quedan 10 para que el sistema cancele tu reserva.",tk);
                    }else{
                        t.cancel();
                        onDestroy();
                    }
                }

                if(contador == 4){
                    if(estadoReserva.equals("Reservado")){
                        task.doInBackground("NOTIFICACION","Atenciòn...","Han pasado 20 minutos desde el momento de la reserva te quedan 5 para que el sistema cancele tu reserva.",tk);
                    }else{
                        t.cancel();
                        onDestroy();
                    }
                }

                if(contador == 5){
                    if(estadoReserva.equals("Reservado")){
                        task.doInBackground("NOTIFICACION","Atenciòn...","El sistema ha cancelado tu reserva, vuelve pronto...",tk);
                        task.doInBackground("LIBERAR_PARQUEADERO",conjunto,parqueadero,tipoParquedero);
                    }
                    t.cancel();
                    onDestroy();
                }
            }
        },5000,  periodo);
    }


    @Override
    public void onDestroy() {
        //Toast.makeText(getApplicationContext(), "Check Parking finalizado", Toast.LENGTH_SHORT).show();
    }
}
