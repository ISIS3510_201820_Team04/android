package com.co.rere;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.co.com.co.dto.Perfil;
import com.co.com.co.dto.PerfilDto;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Timer;
import java.util.TimerTask;

import services.parametros;

public class SessionActivity extends AppCompatActivity {

    private static final String  Tag = "algunacosa";
    private ProgressBar spinner;
    private boolean parametro=false;
    private  boolean bandera= false;

    String deviceToken= null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session);
        spinner = (ProgressBar)findViewById(R.id.progressBar1);
        spinner.setVisibility(View.VISIBLE);
        parametro= getIntent().getExtras() == null ?false:true;
        SharedPreferences prefs = getSharedPreferences("username", MODE_PRIVATE);
        deviceToken = prefs.getString("username", null);
        star();

    }

    private void star(){

        if(estadoConexión()) {
            if (deviceToken != null) {
                if(parametro){
                    String putEstra = getIntent().getExtras().getString("eliminar");
                    if(putEstra!=null){

                        if(putEstra.equals("true")){
                            clearAll();
                            goLogin();
                        }else{
                            goActivityHome(deviceToken);
                        }
                    }else{

                        if (getIntent().getExtras() != null) {
                            for (String key : getIntent().getExtras().keySet()) {
                                Object value = getIntent().getExtras().get(key);
                                Log.d(Tag, "Key: " + key + " Value: " + value);
                                if(key.equals("topic")){
                                    switch (value.toString()){
                                        case "reserva":
                                            redirectActivity(AceptarActivity.class,null);
                                            break;
                                        case "confirmacion":
                                            parametros para = new parametros();
                                            for (String key1 : getIntent().getExtras().keySet()) {
                                                Object value1 = getIntent().getExtras().get(key1);
                                                Log.d(Tag, "Key: " + key + " Value: " + value1);


                                                if(key1.equals("parqueadero")){
                                                    para.setParqueadero(value1.toString());
                                                }
                                                if(key1.equals("nickname")){
                                                    para.setNickname(value1.toString());
                                                }
                                                if(key1.equals("conjunto")){
                                                    para.setConjunto(value1.toString());
                                                }
                                                if(key1.equals("tipoparqueadero")){
                                                    para.setTipoparqueadero(value1.toString());
                                                }
                                            }
                                            redirectActivity(MapsActivity.class, para);
                                            break;
                                    }
                                }
                            }
                        }else {
                            goActivityHome(deviceToken);
                        }

                    }
                }else {
                    goActivityHome(deviceToken);
                }
            }else{
                goLogin();
            }
        }else{
            if(bandera==false){
                bandera = true;
                intentoDeReconexion();
            }

        }
    }

    private void goLogin(){
        Intent login = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(login);
    }

    private void goActivityHome(final String email){


        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("Perfiles");

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                boolean bandera = false;
                for (DataSnapshot messageSnapshot : dataSnapshot.getChildren()) {
                    String user = messageSnapshot.getKey();
                    String passwordc = (String) messageSnapshot.child("pass").getValue();
                    if (email.equals(user)) {
                        PerfilDto perfil = messageSnapshot.getValue(PerfilDto.class);
//                            MyFirebaseInstanceIDService mi = new MyFirebaseInstanceIDService();
//                            perfil.setTokenFireBase(mi.getTokenFireBase());
                        Perfil.setProfile(perfil);
                        Perfil.getProfile().setNickname(user);
                        Perfil.getProfile().setVista(false);
//                            PerfilDao update = new PerfilDao("");
//                            update.actualizar(perfil,perfil.getNickname());

                        SharedPreferences.Editor setuser = getSharedPreferences("username",MODE_PRIVATE).edit();
                        setuser.putString("username",user);
                        setuser.apply();

                        bandera = true;
                        Intent home = new Intent(getApplicationContext(), HomeActivity.class);
                        startActivity(home);
                        break;
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

    }

    private void clearAll(){
        SharedPreferences.Editor setuser = getSharedPreferences("username",MODE_PRIVATE).edit();
        setuser.remove("username");
        Perfil.clearPerfil();
        setuser.apply();
    }

    private final boolean estadoConexión() {
        boolean enabled = true;

        ConnectivityManager connectivityManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = null;
        if (connectivityManager != null) {
            info = connectivityManager.getActiveNetworkInfo();
        }

        if ((info == null || !info.isConnected() || !info.isAvailable())) {
            if(!bandera){
                Toast.makeText(getApplicationContext(), "NO HAY CONEXIÓN A INTERNET LA APP SE CERRARA DESPUES DE DOS INTENTOS DE CONEXIÒN ", Toast.LENGTH_LONG).show();
            }
            enabled = false;
        }else{
            bandera = false;
        }

        return enabled;
    }

    int contador=0;
    private void intentoDeReconexion(){
        final Context ctx = getApplicationContext();
        final Toast toast = Toast.makeText(getApplicationContext(), "Intentando reconectar...", Toast.LENGTH_SHORT);
        final Timer t= new Timer();
        t.scheduleAtFixedRate(new TimerTask(){
            @Override
            public void run(){
                if(bandera){
                    a();
                    Log.i("tag", "A Kiss every 5 seconds");
                    star();
                    contador++;
                    toast.show();
                }else{
                    t.cancel();
                }

            }
        },1000,  5000);


    }

    private  void  a(){
        if(contador==5){
            finish();
            System.exit(0);
        }
    }

    private void redirectActivity(Class<?> activity, parametros para){
        Intent redirec = new Intent(getApplicationContext(),activity);
        if(para!=null){
            redirec.putExtra("parqueadero", para.getParqueadero());
            redirec.putExtra("nickname", para.getNickname());
            redirec.putExtra("conjunto", para.getConjunto());
            redirec.putExtra("tipoparqueadero", para.getTipoparqueadero());
        }
        startActivity(redirec);
    }
}