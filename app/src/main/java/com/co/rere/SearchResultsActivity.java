package com.co.rere;
//import android.app.ActionBar;
import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.app.ActionBar;

import java.util.ArrayList;

public class SearchResultsActivity extends AppCompatActivity {

    public static TextView txtQuery;
    public static String query;
    ListView listview;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_results);

        query = "";
        listview = findViewById(R.id.listViewBB);

        // get the action bar
        ActionBar actionBar = getSupportActionBar();



      // Enabling Back navigation on Action Bar icon
      actionBar.setDisplayHomeAsUpEnabled(true);

      txtQuery = (TextView) findViewById(R.id.txtQuery);

      handleIntent(getIntent());

    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    /**
     * Handling intent data
     */
    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            query = intent.getStringExtra(SearchManager.QUERY);

            /**
             * Use this query to display search results like
             * 1. Getting the data from SQLite and showing in listview
             * 2. Making webrequest and displaying the data
             * For now we just display the query only
             */
            txtQuery.setText("Search Query: " + query);

            //------------- Scrollbar de los conjuntos ---------- //


            ArrayList<String> list = new ArrayList<String>();

            for (int i = 0; i < LoginConjuntoActivity.conjuntos.size(); ++i) {

                if (!list.contains(LoginConjuntoActivity.conjuntos.get(i))) {
                    if (LoginConjuntoActivity.conjuntos.get(i).toLowerCase().indexOf(query.toLowerCase())!=-1)
                        list.add(LoginConjuntoActivity.conjuntos.get(i));
                }
            }


            //  Toast.makeText(context, "lista: "+list.toString(), Toast.LENGTH_SHORT).show();

            final ArrayAdapter adapter = new ArrayAdapter(SearchResultsActivity.this, android.R.layout.simple_list_item_1, list);
            listview.setAdapter(adapter);


            // ListView Item Click Listener
            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {

                    // ListView Clicked item index
                    int itemPosition = position;
                    // ListView Clicked item value
                    String itemValue = (String) listview.getItemAtPosition(position);

                    // Show Alert
                    //   Toast.makeText(getApplicationContext(), "Position :"+itemPosition+"  ListItem : " +itemValue , Toast.LENGTH_LONG).show();


                    LoginConjuntoActivity.conjuntoSeleccionado = itemValue;
                    //Toast.makeText(getApplicationContext(), conjuntoSeleccionado, Toast.LENGTH_LONG).show();
                    Intent mainIntent = new Intent().setClass(SearchResultsActivity.this, CeladorActivity.class);
                    startActivity(mainIntent);
                    finish();

                }

            });



        }

    }



    public static void buscar(final ArrayAdapter adapter){



        txtQuery.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                adapter.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });
    }


}