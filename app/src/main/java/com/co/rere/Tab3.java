package com.co.rere;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.co.com.co.dto.Perfil;
import com.co.com.co.dto.VehiculoDto;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Tab3 extends Fragment {

    EditText nomb;
    EditText apeb;
    TextView conjub;
    EditText cedb;
    TextView aptob;
    EditText marcab;
    EditText modelob;
    EditText placab;
    EditText emailb;
    String nombre;
    String apellido;
    String edificio;
    String apto;
    String cedula;
    String email;
    String marca;
    String modelo;
    String placa;

    View infla;

    FloatingActionButton off;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        nombre = Perfil.getProfile().getNombre();
        apellido = Perfil.getProfile().getApellido();
        edificio = Perfil.getProfile().getConjunto();
        email = Perfil.getProfile().getEmail();
        if (Perfil.getProfile().getApto() == null) {
            apto = "--";
        } else {
            apto = Perfil.getProfile().getApto();
        }
        cedula = Perfil.getProfile().getCedula();
        if (Perfil.getProfile().getVehiculo() == null) {
            marca = "--";
            modelo = "--";
            placa = "--";
        } else {
            marca = Perfil.getProfile().getVehiculo().getMarca();
            modelo = Perfil.getProfile().getVehiculo().getModelo();
            placa = Perfil.getProfile().getVehiculo().getPlaca();
        }

        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        infla = inflater.inflate(R.layout.tab3, container, false);
        nomb = infla.findViewById(R.id.nompr);
        apeb = infla.findViewById(R.id.apepr);
        conjub = infla.findViewById(R.id.conjpr);
        aptob = infla.findViewById(R.id.aptopr);
        cedb = infla.findViewById(R.id.cedpr);
        emailb = infla.findViewById(R.id.emailpr);
        marcab = infla.findViewById(R.id.marcpr);
        modelob = infla.findViewById(R.id.modelpr);
        placab = infla.findViewById(R.id.plapr);
        nomb.setText(nombre);
        apeb.setText(apellido);
        conjub.setText(edificio);
        aptob.setText(apto);
        cedb.setText(cedula);
        emailb.setText(email);
        marcab.setText(marca);
        modelob.setText(modelo);
        placab.setText(placa);

        Button actualizar = infla.findViewById(R.id.actualizar_button);
        actualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (estadoConexión())
                    attemptactualizar();
                else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(infla.getContext());
                    alert.setTitle("Sin Conexion!")
                            .setMessage("No tiene conexión a internet, por favor conectese a una red y vuelva a intentarlo")
                            .setPositiveButton("si se puede", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Log.d("Login", "Sin Internet");
                                }
                            })
                            .show();
                }

            }
        });

        FloatingActionButton off = infla.findViewById(R.id.fab2);
        off.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(infla.getContext());
                alert.setTitle("Ojo!")
                        .setMessage("Realmente quieres cerra la sesión?")
                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                exitApp();
                            }
                        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                }).show();
            }
        });

        return infla;

    }


    public void exitApp(){
        getActivity().finish();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef2 = database.getReference("Perfiles");
        Perfil.clearPerfil();
//        myRef2.child(Perfil.getProfile().getNickname()).child("Login").setValue("NO");
        Intent home = new Intent(infla.getContext(), SessionActivity.class);
        home.putExtra("eliminar","true");
        startActivity(home);

    }

    public void attemptactualizar() {
        final String nombc = nomb.getText().toString();
        final String apec = apeb.getText().toString();
        final String conjuc = conjub.getText().toString();
        final String aptoc = aptob.getText().toString();
        final String cedc = cedb.getText().toString();
        final String emailc = emailb.getText().toString();
        final String marcc = marcab.getText().toString();
        final String modeloc = modelob.getText().toString();
        final String placac = placab.getText().toString();

        if (!nombc.equals("") && !apec.equals("") && !cedc.equals("") && !emailc.equals("") && !marcc.equals("") && !modeloc.equals("") && !placac.equals("")) {
            if (isEmailValid(emailc)) {
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                final DatabaseReference myRef = database.getReference("Perfiles");
//                final DatabaseReference myRef2 = database.getReference("Conjuntos");

                myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        myRef.child(Perfil.getProfile().getNickname()).child("nombre").setValue(nombc);
                        myRef.child(Perfil.getProfile().getNickname()).child("apellido").setValue(apec);
//                        myRef2.child(conjuc).child("aptos").child(aptoc).child("habitantes").child(Perfil.getProfile().getNickname()).child("nickname").setValue(Perfil.getProfile().getNickname());
                        myRef.child(Perfil.getProfile().getNickname()).child("cedula").setValue(cedc);
                        myRef.child(Perfil.getProfile().getNickname()).child("email").setValue(emailc);
                        VehiculoDto vehi = new VehiculoDto();
                        vehi.setMarca(marcc);
                        myRef.child(Perfil.getProfile().getNickname()).child("vehiculo").child("marca").setValue(marcc);
                        vehi.setModelo(modeloc);
                        myRef.child(Perfil.getProfile().getNickname()).child("vehiculo").child("modelo").setValue(modeloc);
                        vehi.setPlaca(placac);
                        Perfil.getProfile().setVehiculo(vehi);
                        myRef.child(Perfil.getProfile().getNickname()).child("vehiculo").child("placa").setValue(placac);
                        AlertDialog.Builder alert = new AlertDialog.Builder(infla.getContext());
                        alert.setTitle("Hecho!")
                                .setMessage("Has sido actualizado!")
                                .setPositiveButton("Si se puede", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Intent home = new Intent(getContext(), HomeActivity.class);
                                        startActivity(home);
                                    }
                                })
                                .show();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        System.out.println("The read failed: " + databaseError.getCode());
                    }
                });
            } else {
                AlertDialog.Builder alert = new AlertDialog.Builder(infla.getContext());
                alert.setTitle("Cambia!")
                        .setMessage("El email no tiene el formato correcto; ejemplo@ejemplo.com")
                        .setPositiveButton("Si se puede", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Log.d("Login", "Error email invalido...");
                            }
                        })
                        .show();
            }
        } else {
            AlertDialog.Builder alert = new AlertDialog.Builder(infla.getContext());
            alert.setTitle("Completa!")
                    .setMessage("No ha completado todos los campos")
                    .setPositiveButton("Si se puede", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Log.d("Login", "Error campos incompletos...");
                        }
                    })
                    .show();
        }
    }

    public boolean isEmailValid(String email) {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches())
            return true;
        else
            return false;
    }

    private boolean estadoConexión() {
        boolean enabled = true;

        ConnectivityManager connectivityManager = (ConnectivityManager) infla.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = null;
        if (connectivityManager != null) {
            info = connectivityManager.getActiveNetworkInfo();
        }

        if ((info == null || !info.isConnected() || !info.isAvailable())) {
//            Toast.makeText(LoginActivity.this, "NO HAY CONEXIÓN A INTERNET ", Toast.LENGTH_LONG).show();
            enabled = false;
        }
        return enabled;
    }

}

