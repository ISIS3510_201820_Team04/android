package com.co.rere;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.co.com.co.dto.Perfil;


public class HomeActivity extends FragmentActivity implements SensorEventListener {
    FloatingActionButton entry;

    SensorManager mSensorManager;
    Sensor mAccelerometer;
    float mAccel;
    float mAccelCurrent;
    float mAccelLast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        FragmentTabHost tabHost = findViewById(android.R.id.tabhost);
        tabHost.setup(this,
                getSupportFragmentManager(), android.R.id.tabcontent);
        tabHost.addTab(tabHost.newTabSpec("tab1").setIndicator("INICIO"),
                Tab1.class,null);
        tabHost.addTab(tabHost.newTabSpec("tab2").setIndicator("CONTACTOS"),
                Tab2.class, null);
        tabHost.addTab(tabHost.newTabSpec("tab3").setIndicator("PERFIL"),
                Tab3.class, null);

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        assert mSensorManager != null;
        mAccelerometer = mSensorManager
                .getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorManager.registerListener(this, mAccelerometer,
                SensorManager.SENSOR_DELAY_UI, new Handler());

        entry =  findViewById(R.id.fab);
        entry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v
            ) {
                Intent intent = new Intent(getApplicationContext(), CitaActivity.class);
                startActivity(intent);
            }
        });
    }


    @Override
    public void onBackPressed() {
        //this.finish();
        new AlertDialog.Builder(this)
                .setTitle("Saliendo de la Sesión")
                .setMessage("¿Seguro quieres salir?")
                .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        finish();
                        Intent home = new Intent(getApplicationContext(), SessionActivity.class);
                        home.putExtra("eliminar","true");
                        startActivity(home);
                    }
                }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        }).show();
    }

    public void closeApplication() {
        finish();
        moveTaskToBack(true);
        System.exit(0);
    }

    private boolean estadoConexión() {
        boolean enabled = true;

        ConnectivityManager connectivityManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = null;
        if (connectivityManager != null) {
            info = connectivityManager.getActiveNetworkInfo();
        }

        if ((info == null || !info.isConnected() || !info.isAvailable())) {
            Toast.makeText(HomeActivity.this, "NO HAY CONEXIÓN A INTERNET ", Toast.LENGTH_LONG).show();
            enabled = false;
        }
        return enabled;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float x = event.values[0];
        float y = event.values[1];
        float z = event.values[2];
        mAccelLast = mAccelCurrent;
        mAccelCurrent = (float) Math.sqrt((double) (x * x + y * y + z * z));
        float delta = mAccelCurrent - mAccelLast;
        mAccel = mAccel * 0.9f + delta;

        if (mAccel > 40) {
            sendActivity();
        }
    }

    private void sendActivity(){
        if(Perfil.getProfile()!=null){
            if(!Perfil.getProfile().isVista()){
                Perfil.getProfile().setVista(true);
                Intent inte = new Intent(getApplicationContext(), CitaActivity.class);
                startActivity(inte);
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
