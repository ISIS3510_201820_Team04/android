package com.co.rere;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.co.com.co.dto.PerfilDto;
import com.co.fragments.CeladorContadorFragment;
import com.co.fragments.CeladorSearchFragment;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class CeladorParqueaderos extends AppCompatActivity {


    TextView placa;
    TextView llegada;
    TextView lugarVisita;
    TextView parqueaderoOcupado;
    TextView tituloLugarVisita;
    private Snackbar snackbar;
    private ConstraintLayout parentView;

    DatabaseReference mDatabase_parqueaderoPropietarios;
    DatabaseReference mDatabase_parqueaderoVisitantes;


    public static String estado;
    public static String tipo;
    public static String lugar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_celador_parqueaderos);

        parentView = (ConstraintLayout) findViewById(R.id.contadorLayout2);
        estado="";
        tipo="";
        lugar="";

        if (connectionState()) {

        } else if (!connectionState()) {
            //Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT);
            //toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            //toast.show();
            showAlertInternet();
            showSnackBarInternet();
        }



        placa = findViewById(R.id.textView17b);
        llegada = findViewById(R.id.textView19b);
        parqueaderoOcupado = findViewById(R.id.textView20b);
        tituloLugarVisita = findViewById(R.id.textView18b);
        lugarVisita = findViewById(R.id.textView21b);



        placa.setText(CeladorContadorFragment.parqueaderoSeleccionado);


//-----------------------------------------------------------//
        //-------------- Firebase ----------------------------------//
        //---------------------------------------------------------//

        //-----------Inicio consulta parqueaderos Visitantes----------//
        mDatabase_parqueaderoVisitantes = FirebaseDatabase.getInstance().getReference().child("Conjuntos/" + LoginConjuntoActivity.conjuntoSeleccionado + "/ParqueaderosVisitantes");
        mDatabase_parqueaderoVisitantes.addValueEventListener(new ValueEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                for (DataSnapshot childSnapShot : dataSnapshot.getChildren()) {

                    if (childSnapShot.getKey().equals(CeladorContadorFragment.parqueaderoSeleccionado)) {

                        estado = childSnapShot.child("Estado").getValue().toString();

                        tipo = "Visitantes";


                        if(childSnapShot.child("Estado").getValue().toString().equals("Ocupado"))
                            lugar = childSnapShot.child("apto").getValue().toString();

                        else
                            tituloLugarVisita.setText("");


                        llegada.setText(estado);
                        parqueaderoOcupado.setText(tipo);
                        lugarVisita.setText(lugar);




                    }

                }

                if(!tipo.equals("Visitantes")) {

                    //-----------Inicio consulta parqueaderos Propietarios----------//
                    mDatabase_parqueaderoPropietarios = FirebaseDatabase.getInstance().getReference().child("Conjuntos/" + LoginConjuntoActivity.conjuntoSeleccionado + "/ParqueaderosPropietarios");
                    mDatabase_parqueaderoPropietarios.addValueEventListener(new ValueEventListener() {
                        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot2) {


                            for (DataSnapshot childSnapShot : dataSnapshot2.getChildren()) {

                                if (childSnapShot.getKey().equals(CeladorContadorFragment.parqueaderoSeleccionado)) {

                                    estado = childSnapShot.child("Estado").getValue().toString();

                                    tipo = "Propietario";

                                    lugar = childSnapShot.child("apto").getValue().toString();



                                    llegada.setText(estado);
                                    parqueaderoOcupado.setText(tipo);
                                    lugarVisita.setText(lugar);




                                }

                            }


                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Toast.makeText(CeladorParqueaderos.this, "Cancelado " + databaseError.toException(), Toast.LENGTH_LONG).show();

                        }

                    });

                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(CeladorParqueaderos.this, "Cancelado " + databaseError.toException(), Toast.LENGTH_LONG).show();

            }

        });






    }



    private boolean connectionState() {
        boolean enabled = true;

        Activity activity = this;
        if (activity != null) {
            ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo info = null;
            if (connectivityManager != null) {
                info = connectivityManager.getActiveNetworkInfo();
            }

            if ((info == null || !info.isConnected() || !info.isAvailable())) {
                //  Toast toast = Toast.makeText(activity, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT);
                // toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                //toast.show();
                enabled = false;
            }
            return enabled;
        } else {
            return false;
        }
    }

    private void showAlertInternet() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Alerta");
        alertDialog.setMessage("No hay conexión a internet, verifique su conexión y vuelva a intentar");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Entendido",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        //finish();
                    }
                });
        alertDialog.show();
    }

    private void showSnackBarInternet() {

        snackbar = Snackbar
                .make(parentView, R.string.no_internet, Snackbar.LENGTH_INDEFINITE);

// Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.RED);
        snackbar.show();
    }

}
