package com.co.rere;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.co.com.co.dto.PerfilDto;
import com.co.dao.PerfilDao;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    public static String conjuntoSeleccionado;
    public Spinner dropdown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        dropdown = findViewById(R.id.conjuntor);
        dropdown.setOnItemSelectedListener(this);

        final ArrayList<String> conjuntos = new ArrayList<>();

        if(estadoConexión())
        {
            conjuntos.add("Seleccione:");

            FirebaseDatabase database1 = FirebaseDatabase.getInstance();
            DatabaseReference myRef1 = database1.getReference("Conjuntos");

            myRef1.addListenerForSingleValueEvent(new ValueEventListener() {
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot messageSnapshot : dataSnapshot.getChildren()) {
                        String conjun = messageSnapshot.getKey();
                        conjuntos.add(conjun);
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, conjuntos);
                    dropdown.setAdapter(adapter);

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    System.out.println("The read failed: " + databaseError.getCode());
                }
            });
        }
        else
        {
            String no[] = new String[1];
            no[0] = "No hay conexión";
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, no);
            dropdown.setAdapter(adapter);
        }

        
        Button entry = findViewById(R.id.register2_button);
        entry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (estadoConexión())
                    attemptRegister();
                else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(RegisterActivity.this);
                    alert.setTitle("Sin Conexion!")
                            .setMessage("No tiene conexión a internet, por favor conectese a una red y vuelva a intentarlo")
                            .setPositiveButton("si se puede", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Log.d("Login", "Sin Internet");
                                }
                            })
                            .show();
                }
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view,
                               int position, long id) {

        conjuntoSeleccionado =  parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView)
    {

    }

    private void attemptRegister() {
        final EditText campoUser = findViewById(R.id.username);
        final EditText campoEmail = findViewById(R.id.email);
        final EditText campoCedula = findViewById(R.id.identificacion);
        final EditText campoPassword = findViewById(R.id.password);
        final EditText campoPassword2 = findViewById(R.id.password2);
        final EditText campoName = findViewById(R.id.nombr);
        final EditText campoApe = findViewById(R.id.aper);

        final String nickname = campoUser.getText().toString();
        final String email = campoEmail.getText().toString();
        final String cedula = campoCedula.getText().toString();
        final String password = campoPassword.getText().toString();
        final String password2 = campoPassword2.getText().toString();
        final String nombre = campoName.getText().toString();
        final String apellido = campoApe.getText().toString();

        if (!nickname.equals("") && !email.equals("") && !cedula.equals("") && !password.equals("")  && !password2.equals("") && !nombre.equals("") && !apellido.equals("") && !conjuntoSeleccionado.equals("") && !conjuntoSeleccionado.equals("Seleccione:") && !conjuntoSeleccionado.equals("No hay conexión")) {
            if (password.equals(password2)) {
                if(isEmailValid(email)) {
                    FirebaseDatabase database = FirebaseDatabase.getInstance();
                    DatabaseReference myRef = database.getReference("Perfiles");

                    myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            boolean bandera = false;
                            for (DataSnapshot messageSnapshot : dataSnapshot.getChildren()) {
                                String user = messageSnapshot.getKey();
                                String email2 = (String) messageSnapshot.child("email").getValue();
                                String ced2 = messageSnapshot.child("cedula").getValue().toString();
                                if (nickname.equals(user) || email.equals(email2) || cedula.equals(ced2)) {
                                    bandera = true;
                                    AlertDialog.Builder alert = new AlertDialog.Builder(RegisterActivity.this);
                                    alert.setTitle("Ups!")
                                            .setMessage("Ya Existe un usuario con ese corrreo o nickname o cedula")
                                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    Log.d("Login", "Error en email o nickname  ya existe...");
                                                }
                                            })
                                            .show();

                                    break;
                                }
                            }
                            if (!bandera) {
                                Intent rega = new Intent(getApplicationContext(), RegAptoActivity.class);
                                rega.putExtra("conjunto", conjuntoSeleccionado);
                                rega.putExtra("nickname", nickname);
                                rega.putExtra("email", email);
                                rega.putExtra("cedula", cedula);
                                rega.putExtra("contraseña", password);
                                rega.putExtra("nombre", nombre);
                                rega.putExtra("apellido", apellido);
                                startActivity(rega);
                            }


                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            System.out.println("The read failed: " + databaseError.getCode());
                        }
                    });
                }
                else
                {
                    AlertDialog.Builder alert = new AlertDialog.Builder(RegisterActivity.this);
                    alert.setTitle("Cambia!")
                            .setMessage("El email no tiene el formato correcto; ejemplo@ejemplo.com")
                            .setPositiveButton("Si se puede", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Log.d("Login", "Error email invalido...");
                                }
                            })
                            .show();
                }
            } else {
                AlertDialog.Builder alert = new AlertDialog.Builder(RegisterActivity.this);
                alert.setTitle("Cambia!")
                        .setMessage("Las contraseñas son diferentes")
                        .setPositiveButton("Si se puede", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Log.d("Login", "Error contraseñas diferentes...");
                            }
                        })
                        .show();
            }
        } else {
            AlertDialog.Builder alert = new AlertDialog.Builder(RegisterActivity.this);
            alert.setTitle("Completa!")
                    .setMessage("No ha completado todos los campos")
                    .setPositiveButton("Si se puede", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Log.d("Login", "Error campos incompletos...");
                        }
                    })
                    .show();
        }
    }


    public boolean isEmailValid(String email)
    {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        +"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        +"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        +"([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(regExpn,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if(matcher.matches())
            return true;
        else
            return false;
    }

    private boolean estadoConexión() {
        boolean enabled = true;

        ConnectivityManager connectivityManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = null;
        if (connectivityManager != null) {
            info = connectivityManager.getActiveNetworkInfo();
        }

        if ((info == null || !info.isConnected() || !info.isAvailable())) {
            Toast.makeText(RegisterActivity.this, "NO HAY CONEXIÓN A INTERNET ", Toast.LENGTH_LONG).show();
            enabled = false;
        }
        return enabled;
    }
}
