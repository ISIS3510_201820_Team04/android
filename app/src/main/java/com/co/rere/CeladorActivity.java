package com.co.rere;


import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.co.adapters.CeladorAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;


public class CeladorActivity extends AppCompatActivity {

    public static String leido;

    String parqueaderoQR;
    String visitanteQR;
    String tipop;
    String conjuntop;
    String eos;

    DatabaseReference mDatabase_perfiles;
    DatabaseReference mDatabase_perfiles2;
    DatabaseReference mDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_celador);

        leido = "Vuelva a intentar";
        parqueaderoQR ="";
        visitanteQR="";

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);

        // Create an adapter that knows which fragment should be shown on each page
        CeladorAdapter adapter = new CeladorAdapter(this, getSupportFragmentManager());

        // Set the adapter onto the view pager
        viewPager.setAdapter(adapter);

        // Give the TabLayout the ViewPager
        TabLayout tabLayout = (TabLayout) findViewById(R.id.celador_activity_tablayout);
        tabLayout.setupWithViewPager(viewPager);


    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null) {
                Toast.makeText(this, "Scan cancelado", Toast.LENGTH_SHORT).show();

            } else {

                leido= result.getContents();

                if(!leido.equals("Vuelva a intentar") && !leido.equals(""))
                {
                    confirmarVisita(leido);

                    showAlertQR(leido);
                }
                else
                {
                    AlertDialog.Builder alert = new AlertDialog.Builder(CeladorActivity.this);
                    alert.setTitle("Error!")
                            .setMessage("El codigo no ha podido ser leido porfavor vuelva a intentar")
                            .setPositiveButton("si se puede", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Log.d("Login", "qr vacio");
                                }
                            })
                            .show();
                }
            }
        } else {
            // This is important, otherwise the result will not be passed to the fragment
            super.onActivityResult(requestCode, resultCode, data);
        }


    }


    private void showAlertQR(String data) {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(this).create();
        alertDialog.setTitle("Confirmacion");
        alertDialog.setMessage(data);
        alertDialog.setButton(android.app.AlertDialog.BUTTON_POSITIVE, "Entendido",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        //finish();
                    }
                });
        alertDialog.show();
    }

    private void confirmarVisita(String entradaQR)
    {
        //Separar strings
        String[] parts = entradaQR.split(",");
        eos = parts[0];

        if(eos.equals("Entrada"))
        {
            parqueaderoQR = parts[1];
            visitanteQR = parts[2];
            tipop = parts[3];
            conjuntop = parts[4];

            mDatabase_perfiles = FirebaseDatabase.getInstance().getReference().child("Conjuntos/"+ conjuntop + "/" + tipop + "/" + parqueaderoQR);
            mDatabase_perfiles.child("Estado").setValue("Ocupado");
            mDatabase_perfiles.child("Visitante").setValue(visitanteQR);

            if(parts[5]!= null && !parts[5].equals(""))
            {
                String visita = parts[5];
                mDatabase = FirebaseDatabase.getInstance().getReference().child("Visitas/");
                mDatabase.child(visita).child("EstadoVisita").setValue("Proceso");
            }
            else
            {

            }



        }
        else if(eos.equals("Salida"))
        {
            parqueaderoQR = parts[1];
            visitanteQR = parts[2];
            tipop = parts[3];
            conjuntop = parts[4];

            mDatabase_perfiles2 = FirebaseDatabase.getInstance().getReference().child("Conjuntos/"+ conjuntop + "/" + tipop + "/" + parqueaderoQR);
            mDatabase_perfiles2.child("Estado").setValue("Disponible");
            mDatabase_perfiles2.child("Visitante").setValue(visitanteQR);

            if(parts[5]!= null && !parts[5].equals(""))
            {
                String visita = parts[5];
                mDatabase = FirebaseDatabase.getInstance().getReference().child("Visitas/");
                mDatabase.child(visita).child("EstadoVisita").setValue("Terminada");
            }
            else
            {

            }

        }



        //Firebase


        //perfil existe


//        mDatabase = FirebaseDatabase.getInstance().getReference().child("Conjuntos/"+ Perfil.getProfile().getConjunto() + "/" + visitap);

        //mDatabase.child("Estado").setValue("Proceso");
    }



    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Saliendo de la Sesión")
                .setMessage("¿Seguro quieres salir?")
                .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        finish();
                        dialog.dismiss();
                    }
                }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        }).show();

    }



}