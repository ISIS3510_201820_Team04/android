package com.co.rere;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import com.co.com.co.dto.Perfil;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Tab1 extends Fragment {

    TextView nomb;
    TextView conjub;
    String perfilucho;
    String conjuntacho;

    Boolean prestamo;

    Button mEmailSignInButton;
    Button ter;

    String visita;

    Switch toggle;
    Switch toggle2;

    View infla;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        perfilucho = Perfil.getProfile().getNombre() + " " + Perfil.getProfile().getApellido();
        conjuntacho = Perfil.getProfile().getConjunto();
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        infla = inflater.inflate(R.layout.tab1, container, false);
        nomb = (TextView) infla.findViewById(R.id.nomhome);
        conjub = (TextView) infla.findViewById(R.id.infohome);
        nomb.setText(perfilucho);
        conjub.setText(conjuntacho);

        if(estadoConexión())
        {
            mostrar();
        }
        else
        {
            Toast.makeText(infla.getContext(), "NO HAY INTERNET, ALGUNOS DATOS PUEDEN ESTAR DESACTUALIZADOS ", Toast.LENGTH_LONG).show();
        }

        mEmailSignInButton = infla.findViewById(R.id.visitas_button);
        mEmailSignInButton.setVisibility(getView().INVISIBLE);
        mostrarAcep();
        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(infla.getContext(), AceptarActivity.class);
                startActivity(intent);
            }
        });

        ter = infla.findViewById(R.id.visitase_button);
        ter.setVisibility(getView().INVISIBLE);
        mostrarTer();
        ter.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(infla.getContext(), CerrarVistaActivity.class);
                intent.putExtra("visita", visita);
                startActivity(intent);
            }
        } );

        toggle = infla.findViewById(R.id.onoff);
        toggle.setVisibility(View.INVISIBLE);
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @SuppressLint("ResourceAsColor")
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(estadoConexión()) {
                    String btn = isChecked ? "true" : "false";
                    toggle.setTextColor((isChecked?R.color.rojo_claro:R.color.azul_oscuro));
                    String textCheck = isChecked ? "Park Prestado: ".concat(toggle.getTextOn().toString()) : "Park sin prestar : ".concat(toggle.getTextOn().toString());
                    toggle.setText(textCheck);
                    cambiarEstadoPark(btn,toggle.getTextOff().toString());
                }
                else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(infla.getContext());
                    alert.setTitle("Sin Conexion!")
                            .setMessage("No tiene conexión a internet, por favor conectese a una red y vuelva a intentarlo")
                            .setPositiveButton("si se puede", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Log.d("Login", "Sin Internet");
                                }
                            })
                            .show();
                }
            }
        });

        toggle2 = infla.findViewById(R.id.onoff2);
        toggle2.setVisibility(View.INVISIBLE);
        toggle2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @SuppressLint("ResourceAsColor")
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(estadoConexión()) {
                    String btn = isChecked ? "true" : "false";
                    toggle2.setTextColor((isChecked?R.color.rojo_claro:R.color.azul_oscuro));
                    String textCheck = isChecked ? "Park Prestado : ".concat(toggle2.getTextOn().toString()) : "Park sin prestar : ".concat(toggle2.getTextOn().toString());
                    toggle2.setText(textCheck);
                    cambiarEstadoPark(btn, toggle2.getTextOff().toString());
                }
                else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(infla.getContext());
                    alert.setTitle("Sin Conexion!")
                            .setMessage("No tiene conexión a internet, por favor conectese a una red y vuelva a intentarlo")
                            .setPositiveButton("si se puede", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Log.d("Login", "Sin Internet");
                                }
                            })
                            .show();
                }
            }
        });

        return infla;
    }

    public void mostrarAcep()
    {
        if(estadoConexión()) {
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference myRef = database.getReference("Visitas/");

            myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    boolean bandera = false;
                    for (DataSnapshot messageSnapshot : dataSnapshot.getChildren()) {
                        String ids =  messageSnapshot.child("IdRecidente").getValue().toString();
                        String estados =  messageSnapshot.child("EstadoVisita").getValue().toString();

                        if (ids.equals(Perfil.getProfile().getNickname())) {

                            if (estados.equals("Enviada")) {
                                mEmailSignInButton.setVisibility(getView().VISIBLE);
                                break;
                            }
                            //mEmailSignInButton.setVisibility(getView().INVISIBLE);
                        } else {
                            mEmailSignInButton.setVisibility(getView().INVISIBLE);
                        }

                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    System.out.println("The read failed: " + databaseError.getCode());
                }
            });
        }
    }

    public void mostrarTer()
    {
        if(estadoConexión()) {
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference myRef = database.getReference("Visitas/");

            myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    boolean bandera = false;
                    for (DataSnapshot messageSnapshot : dataSnapshot.getChildren()) {
                        String ids =  messageSnapshot.child("IdVisitante").getValue().toString();
                        String estados =  messageSnapshot.child("EstadoVisita").getValue().toString();

                        if (ids.equals(Perfil.getProfile().getNickname())) {

                            if (estados.equals("Proceso")) {
                                visita = messageSnapshot.getKey();
                                ter.setVisibility(getView().VISIBLE);
                                break;
                            }
                            //ter.setVisibility(getView().INVISIBLE);
                        } else {
                            ter.setVisibility(getView().INVISIBLE);
                        }

                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    System.out.println("The read failed: " + databaseError.getCode());
                }
            });
        }
        else
        {

        }

    }

    private void cambiarEstadoPark(final String estado, final String apto){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef2 = database.getReference("Conjuntos/"+ Perfil.getProfile().getConjunto()+"/ParqueaderosPropietarios");
        myRef2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int contador = 0;
                for (DataSnapshot messageSnapshot : dataSnapshot.getChildren()) {
                    String parks =  messageSnapshot.child("apto").getValue().toString();
                    if(apto.equals(parks))
                    {
                            String key = messageSnapshot.getKey();
                            //toggle.setText(key);
                            myRef2.child(key).child("prestamo").setValue(estado);
                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });
    }

    public void mostrar()
    {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef4 = database.getReference("Conjuntos/"+ Perfil.getProfile().getConjunto()+"/ParqueaderosPropietarios/");
        myRef4.addListenerForSingleValueEvent(new ValueEventListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int contador = 0;
                for (DataSnapshot messageSnapshot : dataSnapshot.getChildren()) {
                    String parks =  messageSnapshot.child("apto").getValue().toString();

                        boolean p1 = messageSnapshot.child("prestamo").getValue().toString().equals("true")?true:false;
                        contador++;
                        String textCheck = p1 ? "Park Prestado : "+ contador : "Park sin prestar : "+ contador;
                        if(contador==1) {
                            toggle.setChecked(p1);
                            toggle.setText(textCheck);
                            toggle.setVisibility(View.VISIBLE);
                            toggle.setTextOff(parks);
                            toggle.setTextOn("2");
                            toggle.setTextColor((p1?R.color.rojo_claro:R.color.azul_oscuro));
                        }
                        if(contador==2) {
                            toggle2.setChecked(p1);
                            toggle2.setText(textCheck);
                            toggle2.setVisibility(View.VISIBLE);
                            toggle2.setTextOff(parks);
                            toggle2.setTextOn("3");
                            toggle2.setTextColor((p1?R.color.rojo_claro:R.color.azul_oscuro));
                        }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });
    }

    private boolean estadoConexión() {
        boolean enabled = true;

        ConnectivityManager connectivityManager = (ConnectivityManager) infla.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = null;
        if (connectivityManager != null) {
            info = connectivityManager.getActiveNetworkInfo();
        }

        if ((info == null || !info.isConnected() || !info.isAvailable())) {
            Toast.makeText(infla.getContext(), "NO HAY CONEXIÓN A INTERNET ", Toast.LENGTH_LONG).show();
            enabled = false;
        }
        return enabled;
    }
}


