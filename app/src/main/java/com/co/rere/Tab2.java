package com.co.rere;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.co.com.co.dto.Perfil;
import com.co.com.co.dto.PerfilDto;

import java.util.ArrayList;
import java.util.HashMap;

public class Tab2 extends Fragment {

    ArrayAdapter<String> adap;

    Button agr;

    @Override
    public void onCreate(Bundle savedInstanceState) {



        super.onCreate(savedInstanceState);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View infla = inflater.inflate(R.layout.tab2, container, false);



        ListView lvstring = (ListView) infla.findViewById(R.id.listview);

        final ArrayList<String> list = new ArrayList<String>();

        if(Perfil.getProfile().getContactos() != null)
        {
            for (HashMap.Entry<String, PerfilDto> entry : Perfil.getProfile().getContactos().entrySet()) {
                list.add(entry.getKey());
            }

            //final  ArrayAdapter adapter2 = new ;
            lvstring.setAdapter(new ArrayAdapter(infla.getContext(), R.layout.itemcontacs, R.id.itemContacItem, list));

        }

        agr = (Button) infla.findViewById(R.id.agregarcontacto_button);
        agr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(infla.getContext(), AgContactsActivity.class);
                startActivity(intent);
            }
        });

        return infla;
    }
    


}

