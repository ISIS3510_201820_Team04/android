package com.co.rere;

import android.Manifest;
import android.app.LauncherActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.co.com.co.dto.Perfil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import services.LaunchNavigator;

/**
 * Permite tener acceso a la ubicación actual del usuario
 *
 * @author Paula Alvarado & Felipe Iregui
 * @version 1.0
 */
public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, LocationListener {

    /** Variable que me permite tener acceso al mapa de google */
    private GoogleMap mMap;

    LatLng ll2;
    double lat;
    double lon;
    //va a venir en la not
    String con;
    private LaunchNavigator launch;

    /** Permite colocar una posición visual sobre el mapa */
    Marker marker;

    /** Permite tener el control de las herramientas de geolocalización */
    LocationManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        Button btn = findViewById(R.id.qr);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent qr = new Intent(MapsActivity.this, QrActivity.class);
                qr.putExtra("parqueadero", getIntent().getExtras().getString("parqueadero"));
                qr.putExtra("nickname", getIntent().getExtras().getString("nickname"));
                qr.putExtra("conjunto", getIntent().getExtras().getString("conjunto"));
                qr.putExtra("tipop", getIntent().getExtras().getString("tipoparqueadero"));
                qr.putExtra("visita", getIntent().getExtras().getString("visita"));
                startActivity(qr);
            }
        });

        if(estadoConexión()) {
            con = getIntent().getExtras().getString("conjunto");

            FirebaseDatabase database = FirebaseDatabase.getInstance();
            final DatabaseReference myRef4 = database.getReference("Conjuntos/"+ con +"/Coordenadas");
            myRef4.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String la = dataSnapshot.child("Latitud").getValue().toString();
                    String lo = dataSnapshot.child("Longuitud").getValue().toString();
                    lat = Double.parseDouble(la);
                    lon = Double.parseDouble(lo);
                    ll2 = new LatLng(lat, lon);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    System.out.println("The read failed: " + databaseError.getCode());
                }
            });
        }
        else
        {
            AlertDialog.Builder alert = new AlertDialog.Builder(MapsActivity.this);
            alert.setTitle("Sin Conexion!")
                    .setMessage("No tiene conexión a internet, por favor conectese a una red y vuelva a intentarlo")
                    .setPositiveButton("si se puede", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Log.d("Login", "Sin Internet");
                        }
                    })
                    .show();
        }

        launch = new LaunchNavigator();


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        manager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        assert mapFragment != null;
        mapFragment.getMapAsync(this);


        /*
         *   Verificamos si se tienen los permisos de acceso al GPS
         *   y en caso de no tenerlos, mostramos una ventana para solicitar
         *   este permiso
         * */
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
            manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        /*
         * Solicitud de permiso para mostrar la ubicación actual
         */
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
            mMap.setMyLocationEnabled(true);
        }

        // Habilita el botón que permite dirigirse a la posición actual
        mMap.getUiSettings().isMyLocationButtonEnabled();



        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener()
        {
            @Override
            public void onMapClick(LatLng arg0)
            {
                launch.startNavigation(lat, lon, MapsActivity.this);
            }
        });

    }


    @Override
    public void onLocationChanged(Location location) {
        /*
         *  Una vez obtenida el cambio de la ubicación
         *  llamamos al método drawerMarker para plasmarlo en el mapa
         */
        LatLng ll1 = new LatLng(location.getLatitude(), location.getLongitude());

        drawMarker(ll1, ll2);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    /**
     * Permite pintar sobre el mapa un pin, la ubicación que se desee
     *
     * @param point ubicación a pintar
     */
    private void drawMarker(LatLng point, LatLng point2) {

        if(marker== null)
        {
            mMap.addMarker(new MarkerOptions().position(point).title("Mi ubicación"));
            mMap.addMarker(new MarkerOptions().position(point2).title("Conjunto"));
        }
        if(marker!=null)
        {
            marker.remove();
            mMap.addMarker(new MarkerOptions().position(point).title("Mi ubicación"));
            mMap.addMarker(new MarkerOptions().position(point2).title("Conjunto"));
        }


        /*
         * Me permite realizar un zoom dinámico tipo calle (17) a la posición nueva
         */
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(point, 17));
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            AlertDialog.Builder alert = new AlertDialog.Builder(MapsActivity.this);
            alert.setTitle("Atención!")
                    .setMessage("No puedes salir sin antes leer tu codigo QR")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Log.d("Login", "Sin Internet");
                        }
                    })
                    .show();
        }
        return super.onKeyDown(keyCode, event);
    }

    private boolean estadoConexión() {
        boolean enabled = true;

        ConnectivityManager connectivityManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = null;
        if (connectivityManager != null) {
            info = connectivityManager.getActiveNetworkInfo();
        }

        if ((info == null || !info.isConnected() || !info.isAvailable())) {
            Toast.makeText(MapsActivity.this, "NO HAY CONEXIÓN A INTERNET ", Toast.LENGTH_LONG).show();
            enabled = false;
        }
        return enabled;
    }
}