package com.co.rere;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.co.com.co.dto.Perfil;
import com.co.com.co.dto.PerfilDto;
import com.co.dao.PerfilDao;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;

public class AgContactsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agcontacts);

        Button agregar = (Button) findViewById(R.id.agregar_button);
        agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if(estadoConexión())
                {
                    attemptAgregar();
                }
                else
                {
                    AlertDialog.Builder alert = new AlertDialog.Builder(AgContactsActivity.this);
                    alert.setTitle("Sin Conexion!")
                            .setMessage("No tiene conexión a internet, por favor conectese a una red y vuelva a intentarlo")
                            .setPositiveButton("si se puede", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Log.d("Login", "Sin Internet");
                                }
                            })
                            .show();
                }

            }
        });

    }

    private void attemptAgregar()
    {
        final EditText campocontact =  findViewById(R.id.campoagregar);
        final String contacto = campocontact.getText().toString();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("Perfiles");

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean bandera = false;
                try{
                    for (DataSnapshot messageSnapshot: dataSnapshot.getChildren()) {
                        String email= (String) messageSnapshot.getKey();

                        if(email!=null && !email.isEmpty() && !contacto.equals(""))
                        {
                            if (contacto.equals(email) && !contacto.equals(Perfil.getProfile().getNickname()))
                            {
                                PerfilDto perfil = messageSnapshot.getValue(PerfilDto.class);
                                String nickname = (String) messageSnapshot.getKey();
                                HashMap<String, PerfilDto> arrayContacs = Perfil.getProfile().getContactos();
                                bandera = true;
                                if(arrayContacs!=null){
                                    arrayContacs.put(nickname ,perfil);
                                }else{
                                    arrayContacs = new HashMap<String, PerfilDto>();
                                    arrayContacs.put(nickname ,perfil);
                                }

                                PerfilDao dao = new PerfilDao("");
                                Perfil.getProfile().setContactos(arrayContacs);
                                dao.insertObject(Perfil.getProfile().getNickname(),"contactos", "nickname",nickname);
                                AlertDialog.Builder alert = new AlertDialog.Builder(AgContactsActivity.this);
                                alert.setTitle("Listo!")
                                        .setMessage("El Usuario con ese nickname a sido agregado!")
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                Intent home = new Intent(getApplicationContext(), HomeActivity.class);
                                                startActivity(home);
                                            }
                                        })
                                        .show();

                                break;
                            }
                        }
                    }
                }catch (Exception e ){
                    e.printStackTrace();
                }

                if(!bandera) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(AgContactsActivity.this);
                    alert.setTitle("Que Triste!")
                            .setMessage("El Usuario con ese nickname no se encuentra registrado OO Eres tu mismo OO el campo esta vacio")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Log.d("Agregar", "El usuario con ese nickname no existe");
                                }
                            })
                            .show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

    }

    private boolean estadoConexión() {
        boolean enabled = true;

        ConnectivityManager connectivityManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = null;
        if (connectivityManager != null) {
            info = connectivityManager.getActiveNetworkInfo();
        }

        if ((info == null || !info.isConnected() || !info.isAvailable())) {
            Toast.makeText(AgContactsActivity.this, "NO HAY CONEXIÓN A INTERNET ", Toast.LENGTH_LONG).show();
            enabled = false;
        }
        return enabled;
    }
}
