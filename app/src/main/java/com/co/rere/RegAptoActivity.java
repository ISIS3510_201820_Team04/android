package com.co.rere;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.co.com.co.dto.Perfil;
import com.co.com.co.dto.PerfilDto;
import com.co.dao.PerfilDao;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class RegAptoActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private String aptoSeleccionado;
    private Spinner dropdown2;
    private String conjuntop;
    private String nicknamep;
    private String emailp;
    private String cedulap;
    private String contraseñap;
    private String nombrep;
    private String apellidop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg_apto);

        conjuntop = getIntent().getExtras().getString("conjunto");
        nicknamep = getIntent().getExtras().getString("nickname");
        emailp = getIntent().getExtras().getString("email");
        cedulap = getIntent().getExtras().getString("cedula");
        contraseñap = getIntent().getExtras().getString("contraseña");
        nombrep = getIntent().getExtras().getString("nombre");
        apellidop = getIntent().getExtras().getString("apellido");

        TextView conju = findViewById(R.id.con);
        conju.setText(conjuntop);

        dropdown2 = findViewById(R.id.aptore);
        dropdown2.setOnItemSelectedListener(this);

        String ruta = "Conjuntos/" + conjuntop + "/aptos/";

        final ArrayList<String> aptos = new ArrayList<>();
        if (estadoConexión()) {
            aptos.add("Seleccione:");

            FirebaseDatabase database1 = FirebaseDatabase.getInstance();
            DatabaseReference myRef1 = database1.getReference(ruta);

            myRef1.addListenerForSingleValueEvent(new ValueEventListener() {
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot messageSnapshot : dataSnapshot.getChildren()) {
                        String libre = messageSnapshot.child("habitantes").child("nickname").getValue().toString();
                        if(libre.equals("Libre"))
                        {
                            String apto = messageSnapshot.getKey();
                            aptos.add(apto);
                        }
                    }

                    if(aptos.size()==1)
                    {
                        aptos.add("No hay aptos libres o resgistrados");
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, aptos);
                    dropdown2.setAdapter(adapter);

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    System.out.println("The read failed: " + databaseError.getCode());
                }
            });
        }
        else
        {
            String no[] = new String[1];
            no[0] = "No hay conexión";
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, no);
            dropdown2.setAdapter(adapter);
        }



        Button rere = findViewById(R.id.rere_button);
        rere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (estadoConexión())
                    attemptRegisterF();
                else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(RegAptoActivity.this);
                    alert.setTitle("Sin Conexion!")
                            .setMessage("No tiene conexión a internet, por favor conectese a una red y vuelva a intentarlo")
                            .setPositiveButton("si se puede", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Log.d("Login", "Sin Internet");
                                }
                            })
                            .show();
                }
            }
        });
    }

    private void attemptRegisterF()
    {
        if(!aptoSeleccionado.equals("") && !aptoSeleccionado.equals("Seleccione:") && !aptoSeleccionado.equals("No hay conexión") && !aptoSeleccionado.equals("No hay aptos libres o resgistrados"))
        {
            String ruta3 = "Conjuntos/" + conjuntop + "/aptos/";
            FirebaseDatabase database3 = FirebaseDatabase.getInstance();
            DatabaseReference myRef3 = database3.getReference(ruta3);

            myRef3.child(aptoSeleccionado).child("habitantes").child("nickname").setValue(nicknamep);


            AlertDialog.Builder alert = new AlertDialog.Builder(RegAptoActivity.this);
            alert.setTitle("Felicitaciones!")
                    .setMessage("El usuario a sido registrado correctamente porfavor inicie sesion")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent home = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(home);
                        }
                    })
                    .show();

            connectBD(nicknamep, emailp, cedulap, contraseñap, nombrep, apellidop, conjuntop, aptoSeleccionado);




        }
        else
        {
            AlertDialog.Builder alert = new AlertDialog.Builder(RegAptoActivity.this);
            alert.setTitle("Completa!")
                    .setMessage("No ha completado todos los campos")
                    .setPositiveButton("Si se puede", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Log.d("Login", "Error campos incompletos...");
                        }
                    })
                    .show();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view,
                               int position, long id) {

        aptoSeleccionado =  parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView)
    {

    }

    private void connectBD(String nickname, String email, String cedula, String password, String nombre, String apellido, String conjunto, String apto) {
        PerfilDao daoPerfil = new PerfilDao("");
        PerfilDto dtoPerfil = new PerfilDto();
        dtoPerfil.setEmail(email);
        dtoPerfil.setCedula(cedula);
        dtoPerfil.setPass(password);
        dtoPerfil.setNombre(nombre);
        dtoPerfil.setApellido(apellido);
        dtoPerfil.setConjunto(conjunto);
        dtoPerfil.setApto(apto);
        daoPerfil.insertPerfil(dtoPerfil, nickname);
    }


    private boolean estadoConexión() {
        boolean enabled = true;

        ConnectivityManager connectivityManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = null;
        if (connectivityManager != null) {
            info = connectivityManager.getActiveNetworkInfo();
        }

        if ((info == null || !info.isConnected() || !info.isAvailable())) {
            Toast.makeText(RegAptoActivity.this, "NO HAY CONEXIÓN A INTERNET ", Toast.LENGTH_LONG).show();
            enabled = false;
        }
        return enabled;
    }
}
