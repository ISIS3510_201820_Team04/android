package com.co.rere;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.co.com.co.dto.Perfil;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import services.CheckParking;
import services.Notificacion;
import services.parametros;

public class AceptarActivity extends AppCompatActivity {
    String nombre;
    boolean bandera = false;
    String ced2;
    String tk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aceptar);

        if(estadoConexión())
        {
            mostrar();
        }
        else
        {
            AlertDialog.Builder alert = new AlertDialog.Builder(AceptarActivity.this);
            alert.setTitle("Sin Conexion!")
                    .setMessage("No tiene conexión a internet, por favor conectese a una red y vuelva a intentarlo")
                    .setPositiveButton("si se puede", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Log.d("Login", "Sin Internet");
                        }
                    })
                    .show();
        }

        Button mEmailSignInButton = findViewById(R.id.aceptar_button);
        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if(estadoConexión()) {
                    ateemptAceptar();
                }
                else
                {
                    AlertDialog.Builder alert = new AlertDialog.Builder(AceptarActivity.this);
                    alert.setTitle("Sin Conexion!")
                            .setMessage("No tiene conexión a internet, por favor conectese a una red y vuelva a intentarlo")
                            .setPositiveButton("si se puede", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Log.d("Login", "Sin Internet");
                                }
                            })
                            .show();
                }
            }
        });

    }

    public void mostrar()
    {
        final TextView campoAceptar = findViewById(R.id.aceptar);
        if(estadoConexión()) {
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference myRef = database.getReference("Visitas/");

            myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    boolean bandera = false;
                    for (DataSnapshot messageSnapshot : dataSnapshot.getChildren()) {
                        String ids =  messageSnapshot.child("IdRecidente").getValue().toString();
                        String estados =  messageSnapshot.child("EstadoVisita").getValue().toString();

                        if (ids.equals(Perfil.getProfile().getNickname())) {
                            ced2 = (String) messageSnapshot.child("IdVisitante").getValue();

                            if (estados.equals("Enviada")) {
                                nombre = messageSnapshot.getKey();
                                campoAceptar.setText("Ya viene " + ced2 + " en la  " + nombre);
                                break;
                            }
                            campoAceptar.setText("No tienes visitas");
                        } else {
                            campoAceptar.setText("No tienes visitas");
                        }

                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    System.out.println("The read failed: " + databaseError.getCode());
                }
            });
        }
        else
        {
            campoAceptar.setText("No tienes Internet");
        }
    }

    private void ateemptAceptar()
    {
        if(nombre!=null && !nombre.equals("")) {

            FirebaseDatabase database = FirebaseDatabase.getInstance();
            final DatabaseReference myRef = database.getReference("Visitas/");
            final DatabaseReference ref = database.getReference("Perfiles");

            ref.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    tk = dataSnapshot.child(ced2).child("Token").getValue().toString();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            myRef.child(nombre).child("EstadoVisita").setValue("Aceptada");

            FirebaseDatabase database2 = FirebaseDatabase.getInstance();
            final DatabaseReference myRef2 = database2.getReference("Conjuntos/" + Perfil.getProfile().getConjunto() + "/ParqueaderosVisitantes");
            myRef2.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot messageSnapshot : dataSnapshot.getChildren()) {
                        String estado = messageSnapshot.child("Estado").getValue().toString();
                        if (estado.equals("Disponible")) {
                            bandera = true;
                            final String key = (String) messageSnapshot.getKey();
                            myRef2.child(key).child("Estado").setValue("Reservado");
                            AlertDialog.Builder alert = new AlertDialog.Builder(AceptarActivity.this);
                            alert.setTitle("Aceptada!")
                                    .setMessage(" Tu visita viene en camino y ya se le asigno el parqueadero recuerda que tines 20 minutos para ocuparlo a partir del momento." + "ParquederosVisitantes " + key + "de: " + Perfil.getProfile().getConjunto())
                                    .setPositiveButton("oK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            Intent home = new Intent(getApplicationContext(), HomeActivity.class);
                                            Intent iService = new Intent(getApplicationContext(), CheckParking.class);
                                            iService.putExtra("token", tk);
                                            iService.putExtra("conjunto", Perfil.getProfile().getConjunto());
                                            iService.putExtra("parqueadero", key);
                                            iService.putExtra("tipoParqueadero", "ParqueaderosVisitantes");
                                            iService.setAction("services.action.PARKING_TIME");
                                            getApplicationContext().startService(iService);
                                            startActivity(home);
                                        }
                                    })
                                    .show();

                            Notificacion not = new Notificacion();
                            parametros para = new parametros();
                            para.setTopic("confirmacion");
                            para.setConjunto(Perfil.getProfile().getConjunto());
                            para.setNickname(ced2);
                            para.setParqueadero(key);
                            para.setTipoparqueadero("ParqueaderosVisitantes");
                            not.post(not.createObjNotify("Visita aceptada...","Tu visita ha sido acepta, te esperamos...", tk, para));
                            break;
                        }
                    }

                    if (!bandera)
                    {
                        FirebaseDatabase database3 = FirebaseDatabase.getInstance();
                        final DatabaseReference myRef3 = database3.getReference("Conjuntos/" + Perfil.getProfile().getConjunto() + "/ParqueaderosPropietarios");
                        myRef3.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                for (DataSnapshot messageSnapshot : dataSnapshot.getChildren()) {
                                    String estado = messageSnapshot.child("Estado").getValue().toString();
                                    String prestamo = messageSnapshot.child("prestamo").getValue().toString();
                                    if (estado.equals("Disponible")) {
                                        if (prestamo.equals("true")) {
                                            final String key = (String) messageSnapshot.getKey();
                                            bandera = true;
                                            myRef3.child(key).child("Estado").setValue("Reservado");
                                            AlertDialog.Builder alert = new AlertDialog.Builder(AceptarActivity.this);
                                            alert.setTitle("Aceptada!")
                                                    .setMessage(" Tu visita viene en camino y ya se le asigno el parqueadero " + "ParqueaderosPropietarios " + key + "de: " + Perfil.getProfile().getConjunto())
                                                    .setPositiveButton("oK", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialogInterface, int i) {
                                                            Intent home = new Intent(getApplicationContext(), HomeActivity.class);
                                                            Intent iService = new Intent(getApplicationContext(), CheckParking.class);
                                                            iService.putExtra("token", tk);
                                                            iService.putExtra("conjunto", Perfil.getProfile().getConjunto());
                                                            iService.putExtra("parqueadero", key);
                                                            iService.putExtra("tipoParqueadero", "ParqueaderosPropietarios");
                                                            iService.setAction("services.action.PARKING_TIME");
                                                            getApplicationContext().startService(iService);
                                                            startActivity(home);
                                                        }
                                                    })
                                                    .show();
                                            Notificacion not = new Notificacion();
                                            parametros para = new parametros();
                                            para.setTopic("confirmacion");
                                            para.setConjunto(Perfil.getProfile().getConjunto());
                                            para.setNickname(ced2);
                                            para.setParqueadero(key);
                                            para.setTipoparqueadero("ParqueaderosPropietarios");
                                            para.setVisita(nombre);
                                            not.post(not.createObjNotify("Visita aceptada...","Tu visita ha sido acepta, te esperamos...", tk, para));
                                            break;
                                        }

                                    }
                                }

                                try {
                                    Thread.sleep(2000);
                                    if (!bandera) {
                                        AlertDialog.Builder alert = new AlertDialog.Builder(AceptarActivity.this);
                                        alert.setTitle("Atención!")
                                                .setMessage("No hay parqueaderos disponibles para tu invitado! AVISA")
                                                .setPositiveButton("oK", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        Intent home = new Intent(getApplicationContext(), HomeActivity.class);
                                                        startActivity(home);
                                                    }
                                                })
                                                .show();
                                        Notificacion not = new Notificacion();
                                        parametros para = new parametros();
                                        not.post(not.createObjNotify("Ooops!!!","Te informamos que en el momento no hay parqueaderos disponibles, por favor intenta mas tarde...", tk, para));
                                    }
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }




                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
        else
        {
            AlertDialog.Builder alert = new AlertDialog.Builder(AceptarActivity.this);
            alert.setTitle("Atención!")
                    .setMessage("No tienes visitas en este momento")
                    .setPositiveButton("oK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent home = new Intent(getApplicationContext(), HomeActivity.class);
                            startActivity(home);
                        }
                    })
                    .show();
        }
    }

    private boolean estadoConexión() {
        boolean enabled = true;

        ConnectivityManager connectivityManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = null;
        if (connectivityManager != null) {
            info = connectivityManager.getActiveNetworkInfo();
        }

        if ((info == null || !info.isConnected() || !info.isAvailable())) {
            Toast.makeText(AceptarActivity.this, "NO HAY CONEXIÓN A INTERNET ", Toast.LENGTH_LONG).show();
            enabled = false;
        }
        return enabled;
    }
}