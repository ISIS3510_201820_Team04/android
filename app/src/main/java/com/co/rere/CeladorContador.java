package com.co.rere;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.co.com.co.dto.PerfilDto;
import com.co.fragments.CeladorSearchFragment;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class CeladorContador extends AppCompatActivity {

    TextView placa;
    TextView llegada;
    TextView lugarVisita;
    TextView tituloHoraLLegada;
    TextView tituloParqueaderoOcupado;
    TextView parqueaderoOcupado;
    TextView tituloLugarVisita;
    private Snackbar snackbar;
    private CoordinatorLayout parentView;


    String visitante;

    String ingreso;
    String parqueadero;
    String apto;




    DatabaseReference mDatabase_perfiles;
    DatabaseReference mDatabase_parqueaderoVisitantes;


    int conteo_perfiles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_celador_contador);

        parentView = (CoordinatorLayout) findViewById(R.id.contadorLayout1);

        if (connectionState()) {

        } else if (!connectionState()) {
            //Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT);
            //toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            //toast.show();
            showAlertInternet();
            showSnackBarInternet();
        }



        placa = findViewById(R.id.textView17a);
        tituloHoraLLegada = findViewById(R.id.textView14a);
        llegada = findViewById(R.id.textView19a);
        tituloParqueaderoOcupado = findViewById(R.id.textView15a);
        parqueaderoOcupado = findViewById(R.id.textView20a);
        tituloLugarVisita = findViewById(R.id.textView18a);
        lugarVisita = findViewById(R.id.textView21a);

        ingreso = "";
        parqueadero = "";
        apto = "";
        visitante = "";


            placa.setText(CeladorSearchFragment.placaSeleccionada);

            //-----------------------------------------------------------//
            //-------------- Firebase ----------------------------------//
            //---------------------------------------------------------//

            //---------Inicio consulta perfiles comparado a la placa-----------//

            final FirebaseDatabase database = com.google.firebase.database.FirebaseDatabase.getInstance();
            if (database != null) {

                mDatabase_perfiles = FirebaseDatabase.getInstance().getReference("Perfiles");
                mDatabase_perfiles.addValueEventListener(new ValueEventListener() {
                    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        conteo_perfiles = 0;
                        boolean centinela = false;

                        PerfilDto perfil;

                        for (DataSnapshot childSnapShot : dataSnapshot.getChildren()) {

                            if (childSnapShot.child("vehiculo").getValue() != null) {

                                perfil = childSnapShot.getValue(PerfilDto.class);

                                if (perfil.getVehiculo().getPlaca().equals(CeladorSearchFragment.placaSeleccionada)) {

                                    visitante = childSnapShot.getKey();
                                }

                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Toast.makeText(CeladorContador.this, "Error SOLICITUDES VEH!" + databaseError.toException(), Toast.LENGTH_LONG).show();

                    }

                });

                //------------Fin consulta perfiles---------------//


                //-----------Inicio consulta parqueaderos----------//
                mDatabase_parqueaderoVisitantes = FirebaseDatabase.getInstance().getReference().child("Conjuntos/"+LoginConjuntoActivity.conjuntoSeleccionado+"/ParqueaderosVisitantes");
                mDatabase_parqueaderoVisitantes.addValueEventListener(new ValueEventListener() {
                    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot2) {


                        for (DataSnapshot childSnapShot : dataSnapshot2.getChildren()) {

                            if (childSnapShot.child("Visitante").getValue().equals(visitante)) {

                                ingreso = childSnapShot.child("horaingreso").getValue().toString();
                                parqueadero = childSnapShot.getKey();
                                apto = "Apto " + childSnapShot.child("apto").getValue().toString();

                                llegada.setText(ingreso);
                                lugarVisita.setText(apto);
                                parqueaderoOcupado.setText(parqueadero);

                            }

                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Toast.makeText(CeladorContador.this, "Cancelado "+databaseError.toException(), Toast.LENGTH_LONG).show();

                    }

                });


            }




    }



    private boolean connectionState() {
        boolean enabled = true;

        Activity activity = this;
        if (activity != null) {
            ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo info = null;
            if (connectivityManager != null) {
                info = connectivityManager.getActiveNetworkInfo();
            }

            if ((info == null || !info.isConnected() || !info.isAvailable())) {
                //  Toast toast = Toast.makeText(activity, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT);
                // toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                //toast.show();
                enabled = false;
            }
            return enabled;
        } else {
            return false;
        }
    }

    private void showAlertInternet() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Alerta");
        alertDialog.setMessage("No hay conexión a internet, verifique su conexión y vuelva a intentar");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Entendido",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        //finish();
                    }
                });
        alertDialog.show();
    }

    private void showSnackBarInternet() {

        snackbar = Snackbar
                .make(parentView, R.string.no_internet, Snackbar.LENGTH_INDEFINITE);

// Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.RED);
        snackbar.show();
    }


}
