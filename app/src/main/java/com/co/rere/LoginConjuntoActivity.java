package com.co.rere;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class LoginConjuntoActivity extends AppCompatActivity {

    static Context context;
    private DatabaseReference mDatabase_conjuntos;
    int conteo_conjuntos;
    public static String conjuntoSeleccionado = "";
    public static ArrayList<String> conjuntos = new ArrayList<>();




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_conjunto);

        context = this;
        final ListView listview = findViewById(R.id.listView1);


        //----------Estado Conexion-----------//
        if (connectionState()) {

        } else if (!connectionState()) {
            //Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT);
            //toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            //toast.show();
            showAlertInternet();

        }


        //----------- Firebase -------- //
        //Este método crea un arraylist de todos los conjuntos

        mDatabase_conjuntos = FirebaseDatabase.getInstance().getReference().child("Conjuntos/");
        mDatabase_conjuntos.addValueEventListener(new ValueEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                conteo_conjuntos = (int) dataSnapshot.getChildrenCount();

                for (DataSnapshot childSnapShot : dataSnapshot.getChildren()) {

                    conjuntos.add(childSnapShot.getKey());

                    //  Toast.makeText(context,""+conjuntos.toString(),Toast.LENGTH_LONG).show();

                    // Toast.makeText(context, "Número conjuntos: "+conteo_conjuntos, Toast.LENGTH_SHORT).show();

                }


                //------------- Scrollbar de los conjuntos ---------- //


                ArrayList<String> list = new ArrayList<String>();


                for (int i = 0; i < conjuntos.size(); ++i) {

                    if (!list.contains(conjuntos.get(i)))
                        list.add(conjuntos.get(i));
                }
                //  Toast.makeText(context, "lista: "+list.toString(), Toast.LENGTH_SHORT).show();

                final ArrayAdapter adapter = new ArrayAdapter(context, android.R.layout.simple_list_item_1, list);
                listview.setAdapter(adapter);


                // ListView Item Click Listener
                listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {

                        // ListView Clicked item index
                        int itemPosition = position;
                        // ListView Clicked item value
                        String itemValue = (String) listview.getItemAtPosition(position);

                        // Show Alert
                        //   Toast.makeText(getApplicationContext(), "Position :"+itemPosition+"  ListItem : " +itemValue , Toast.LENGTH_LONG).show();


                        conjuntoSeleccionado = itemValue;
                        //Toast.makeText(getApplicationContext(), conjuntoSeleccionado, Toast.LENGTH_LONG).show();
                        Intent mainIntent = new Intent().setClass(context, CeladorActivity.class);
                        startActivity(mainIntent);
                        finish();

                    }

                });


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(context, "Error SOLICITUDES VEH!" + databaseError.toException(), Toast.LENGTH_LONG).show();
            }

        });


    }


    private boolean connectionState() {
        boolean enabled = true;

        Activity activity = this;
        if (activity != null) {
            ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo info = null;
            if (connectivityManager != null) {
                info = connectivityManager.getActiveNetworkInfo();
            }

            if ((info == null || !info.isConnected() || !info.isAvailable())) {
                //Toast toast = Toast.makeText(activity, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT);
                //toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                //toast.show();
                enabled = false;
            }
            return enabled;
        } else {
            return false;
        }
    }

    private void showAlertInternet() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Alerta");
        alertDialog.setMessage("No hay conexión a internet, verifique su conexión y vuelva a intentar");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Entendido",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        //finish();
                    }
                });
        alertDialog.show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        switch (item.getItemId()) {
            case R.id.search:
                // search action
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_menu, menu);

//        SearchManager searchManager = (SearchManager)
//                getSystemService(Context.SEARCH_SERVICE);
//        searchMenuItem = menu.findItem(R.id.search);
//        searchView = (SearchView) searchMenuItem.getActionView();
//
//        searchView.setSearchableInfo(searchManager.
//                getSearchableInfo(getComponentName()));
//        searchView.setSubmitButtonEnabled(true);
//        searchView.setOnQueryTextListener(this);


        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));


        return super.onCreateOptionsMenu(menu);
    }





}
