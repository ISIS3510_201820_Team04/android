package com.co.rere;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.co.com.co.dto.Perfil;
import com.co.com.co.dto.PerfilDto;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import services.Notificacion;
import services.parametros;


public class CitaActivity extends AppCompatActivity {

    String user;
    EditText campoVisit;
    static int cont;
    String nombre;
    boolean retu;
    String conjuntov;
    boolean banderaReserva=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cita);
        validar();


        Button entry = findViewById(R.id.agendar_button);
        entry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(estadoConexión()) {
                    atemptCita();
                }
                else
                {
                    AlertDialog.Builder alert = new AlertDialog.Builder(CitaActivity.this);
                    alert.setTitle("Sin Conexion!")
                            .setMessage("No tiene conexión a internet, por favor conectese a una red y vuelva a intentarlo")
                            .setPositiveButton("si se puede", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Log.d("Login", "Sin Internet");
                                }
                            })
                            .show();
                }

            }
        });

    }

    private void atemptCita()
    {
        campoVisit = findViewById(R.id.campovisitan);
        final String nickname = campoVisit.getText().toString();

        if(!nickname.equals(""))
        {

            FirebaseDatabase database = FirebaseDatabase.getInstance();
            final DatabaseReference myRef2 = database.getReference("Perfiles");
            final DatabaseReference myRef = database.getReference("Visitas/");

            myRef2.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    boolean bandera = false;
                    for (HashMap.Entry<String, PerfilDto> entry : Perfil.getProfile().getContactos().entrySet()) {
                        if (nickname.equals(entry.getKey())) {

                            conjuntov = dataSnapshot.child(entry.getKey()).child("conjunto").getValue().toString();

                            if(banderaReserva)
                            {
                                String tk = dataSnapshot.child(entry.getKey()).child("Token").getValue().toString();

                                final String conju = (String) dataSnapshot.child(entry.getKey()).child("conjunto").getValue();

                                Notificacion send = new Notificacion();
                                parametros para = new parametros();
                                para.setTopic("reserva");
                                send.post(send.createObjNotify("Atenciòn tines una visita!!!",Perfil.getProfile()+" Quiere visitarte, deseas aceptar!!!",tk,para));

                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
                                Date date = new Date();
                                Calendar rightNow = Calendar.getInstance();
                                int currentHourIn24Format = rightNow.get(Calendar.HOUR_OF_DAY);
                                int minutes = rightNow.get(Calendar.MINUTE);
                                int secs = rightNow.get(Calendar.SECOND);
                                int mili = rightNow.get(Calendar.MILLISECOND);

                                nombre = "Visita" + entry.getKey() + currentHourIn24Format + minutes + secs + mili;

                                String fecha = dateFormat.format(date);
                                bandera = true;
                                myRef.child(nombre).child("EstadoVisita").setValue("Enviada");
                                myRef.child(nombre).child("FechaVisita").setValue(fecha);
                                myRef.child(nombre).child("HoraVisita").setValue(currentHourIn24Format + ":" + minutes);
                                myRef.child(nombre).child("IdRecidente").setValue(entry.getKey());
                                myRef.child(nombre).child("IdVisitante").setValue(Perfil.getProfile().getNickname());
                                myRef.child(nombre).child("Parqueadero").setValue("SI");



                                AlertDialog.Builder alert = new AlertDialog.Builder(CitaActivity.this);
                                alert.setTitle("Corre!")
                                        .setMessage("Se ha agendado correctamente la visita se le avisara cuando sea aceptada")
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                Intent home = new Intent(getApplicationContext(), HomeActivity.class);
                                                startActivity(home);
                                            }
                                        })
                                        .show();
                                break;
                            } else {
                                AlertDialog.Builder alert = new AlertDialog.Builder(CitaActivity.this);
                                alert.setTitle("Para!")
                                        .setMessage("Ya tienes una visita agendada, espera a que sea aceptada")
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                Log.d("visita", "repite visita");
                                            }
                                        })
                                        .show();
                                bandera = true;

                            }

                        }

                    }
                    try {
                        Thread.sleep(3000);
                        if (!bandera) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(CitaActivity.this);
                            alert.setTitle("Ups!")
                                    .setMessage("No tienes ese usuario como contacto")
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            Log.d("Login", "Error en usuario no es contacto...");
                                        }
                                    })
                                    .show();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
        else
        {
            AlertDialog.Builder alert = new AlertDialog.Builder(CitaActivity.this);
            alert.setTitle("Completa!")
                    .setMessage("¿A quién quieres visitar? Llena el campo")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Log.d("Login", "Error en usuario no es contacto...");
                        }
                    })
                    .show();
        }
    }

    private void validar()
    {
        retu = true;
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myR = database.getReference("Visitas");

        myR.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot messageSnapshot : dataSnapshot.getChildren())
                {
                    String estado = messageSnapshot.child("EstadoVisita").getValue().toString();
                    String visi = messageSnapshot.child("IdVisitante").getValue().toString();
                    if(estado.equals("Enviada") && visi.equals(Perfil.getProfile().getNickname()))
                    {
                        banderaReserva = false;
                        break;
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private boolean estadoConexión() {
        boolean enabled = true;

        ConnectivityManager connectivityManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = null;
        if (connectivityManager != null) {
            info = connectivityManager.getActiveNetworkInfo();
        }

        if ((info == null || !info.isConnected() || !info.isAvailable())) {
            Toast.makeText(CitaActivity.this, "NO HAY CONEXIÓN A INTERNET ", Toast.LENGTH_LONG).show();
            enabled = false;
        }
        return enabled;
    }

    @Override
    public void onBackPressed() {
        Perfil.getProfile().setVista(false);
        Intent inte = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(inte);
    }
}
