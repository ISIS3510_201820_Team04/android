package com.co.rere;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.co.com.co.dto.Perfil;
import com.co.com.co.dto.PerfilDto;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;



/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button mEmailSignInButton = findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (estadoConexión()) {

                    attemptLogin();
                } else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(LoginActivity.this);
                    alert.setTitle("Sin Conexión!")
                            .setMessage("No tiene conexión a internet, por favor conéctese a una red y vuelva a intentarlo")
                            .setPositiveButton("si se puede", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Log.d("Login", "Sin Internet");
                                }
                            })
                            .show();
                }

            }
        });

        Button entry = findViewById(R.id.register_button);
        entry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(intent);
            }
        });

        Button celador = findViewById(R.id.vigilante_button);
        celador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LoginConjuntoActivity.class);
                startActivity(intent);
            }
        });


    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        final EditText campoUser = findViewById(R.id.nickname);
        final EditText campoPass = findViewById(R.id.password);
        final String email = campoUser.getText().toString();
        final String password = campoPass.getText().toString();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("Perfiles");

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                boolean bandera = false;
                for (DataSnapshot messageSnapshot : dataSnapshot.getChildren()) {
                    String user = messageSnapshot.getKey();
                    String passwordc = (String) messageSnapshot.child("pass").getValue();
                    if (email.equals(user)) {
                        if (password.equals(passwordc)) {
                            PerfilDto perfil = messageSnapshot.getValue(PerfilDto.class);

                            Perfil.setProfile(perfil);
                            Perfil.getProfile().setNickname(user);

                            SharedPreferences.Editor setuser = getSharedPreferences("username",MODE_PRIVATE).edit();
                            setuser.putString("username",user);
                            setuser.apply();

                            meterToken();
                            bandera = true;
                            Intent home = new Intent(getApplicationContext(), HomeActivity.class);
                            startActivity(home);
                            break;

                        }
                    }
                }

                if (!bandera) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(LoginActivity.this);
                    alert.setTitle("Atención!")
                            .setMessage("Usuario o contraseña errada, valide e intente nuevamente")
                            .setPositiveButton("Si se puede", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Log.d("Login", "Error en usuario o contraseña o usuario no existe...");
                                }
                            })
                            .show();
                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });
    }

    public void meterToken()
    {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("Perfiles");

        MyFirebaseInstanceIDService mi = new MyFirebaseInstanceIDService();
        String token = mi.getTokenFireBase();
        Perfil.getProfile().setTokenFireBase(token);
        myRef.child(Perfil.getProfile().getNickname()).child("Token").setValue(token);
//        myRef.child(Perfil.getProfile().getNickname()).child("Login").setValue("SI");
    }

    @Override
    public void onBackPressed() {
        //this.finish();
        closeApplication();
    }

    public void closeApplication() {
        finish();
        moveTaskToBack(true);
        System.exit(0);
    }

    private boolean estadoConexión() {
        boolean enabled = true;

        ConnectivityManager connectivityManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = null;
        if (connectivityManager != null) {
            info = connectivityManager.getActiveNetworkInfo();
        }

        if ((info == null || !info.isConnected() || !info.isAvailable())) {
            Toast.makeText(LoginActivity.this, "NO HAY CONEXIÓN A INTERNET ", Toast.LENGTH_LONG).show();
            enabled = false;
        }
        return enabled;
    }

}

