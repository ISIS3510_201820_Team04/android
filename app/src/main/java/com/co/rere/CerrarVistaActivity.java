package com.co.rere;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.co.com.co.dto.Perfil;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

public class CerrarVistaActivity extends AppCompatActivity {

    String cvisi;
    String ced2;
    String park;
    String nombre;
    String part0;
    String part1;
    String part3;
    String part4;

    String visi;

    Bitmap bitmap ;
    ImageView imageView;
    public final static int QRcodeWidth = 350 ;
    Button btn;

    boolean bandera = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cerrar_vista);

        visi = getIntent().getExtras().getString("visita");

        if (estadoConexión()) {
            ateemptTerminar();
        } else {
            AlertDialog.Builder alert = new AlertDialog.Builder(CerrarVistaActivity.this);
            alert.setTitle("Sin Conexion!")
                    .setMessage("No tiene conexión a internet, por favor conectese a una red y vuelva a intentarlo")
                    .setPositiveButton("si se puede", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent inte = new Intent(getApplicationContext(), HomeActivity.class);
                            startActivity(inte);
                        }
                    })
                    .show();
        }

    }

    private void ateemptTerminar() {


        part0 = "Salida";

        if(Perfil.getProfile().getConjunto().equals("Torres de la colina"))
        {
            cvisi = "Calleja";
        }
        else if(Perfil.getProfile().getConjunto().equals("Calleja"))
        {
            cvisi = "Torres de la colina";
        }

        part4 = cvisi;
        final FirebaseDatabase database = FirebaseDatabase.getInstance();

        final DatabaseReference myRef4 = database.getReference("Conjuntos/" + cvisi + "/ParqueaderosVisitantes");
        myRef4.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot messageSnapshot : dataSnapshot.getChildren()) {
                    String ids = messageSnapshot.child("Visitante").getValue().toString();
                    String estado = messageSnapshot.child("Estado").getValue().toString();
                    if (ids.equals(Perfil.getProfile().getNickname())) {
                        if (estado.equals("Ocupado")) {
                            bandera = true;
                            park = messageSnapshot.getKey();
                            part1 = park;
                            part3 = "ParqueaderosVisitantes";
                            String editText = part0 + "," + part1 + ",Libre," + part3 + "," + part4 + "," + visi;
                            try {
                                bitmap = TextToImageEncode(editText);
                                imageView.setImageBitmap(bitmap);

                            } catch (WriterException e) {
                                e.printStackTrace();
                            }
                            break;
                        }
                    }

                }

                if (!bandera) {
                    final DatabaseReference myRef5 = database.getReference("Conjuntos/" + cvisi + "/ParqueaderosPropietarios");
                    myRef5.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for (DataSnapshot messageSnapshot : dataSnapshot.getChildren()) {
                                String ids = messageSnapshot.child("Visitante").getValue().toString();
                                String estado = messageSnapshot.child("Estado").getValue().toString();
                                if (ids.equals(Perfil.getProfile().getNickname())) {
                                    if (estado.equals("Ocupado")) {
                                        bandera = true;
                                        park = messageSnapshot.getKey();
                                        part1 = park;
                                        part3 = "ParqueaderosPropietarios";
                                        String editText = part0 + "," + part1 + ",Libre," + part3 + "," + part4;
                                        try {
                                            bitmap = TextToImageEncode(editText);
                                            imageView.setImageBitmap(bitmap);

                                        } catch (WriterException e) {
                                            e.printStackTrace();
                                        }
                                        break;
                                    }
                                } else {

                                }

                            }
                            if(!bandera)
                            {
                                AlertDialog.Builder alert = new AlertDialog.Builder(CerrarVistaActivity.this);
                                alert.setTitle("No estas en visita!")
                                        .setMessage("Este código solo será generado si te encuentras en alguna visita")
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                Intent inte = new Intent(getApplicationContext(), SessionActivity.class);
                                                startActivity(inte);
                                            }
                                        })
                                        .show();
                            }

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            System.out.println("The read failed: " + databaseError.getCode());
                        }
                    });
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

        imageView = findViewById(R.id.imageView2);

        btn = findViewById(R.id.home_button2);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inte = new Intent(getApplicationContext(), SessionActivity.class);
                startActivity(inte);
            }
        });


    }



    Bitmap TextToImageEncode(String Value) throws WriterException {
        BitMatrix bitMatrix;
        try {
            bitMatrix = new MultiFormatWriter().encode(
                    Value,
                    BarcodeFormat.DATA_MATRIX.QR_CODE,
                    QRcodeWidth, QRcodeWidth, null
            );

        } catch (IllegalArgumentException Illegalargumentexception) {

            return null;
        }
        int bitMatrixWidth = bitMatrix.getWidth();

        int bitMatrixHeight = bitMatrix.getHeight();

        int[] pixels = new int[bitMatrixWidth * bitMatrixHeight];

        for (int y = 0; y < bitMatrixHeight; y++) {
            int offset = y * bitMatrixWidth;

            for (int x = 0; x < bitMatrixWidth; x++) {

                pixels[offset + x] = bitMatrix.get(x, y) ?
                        getResources().getColor(R.color.black):getResources().getColor(R.color.white);
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);

        bitmap.setPixels(pixels, 0, 350, 0, 0, bitMatrixWidth, bitMatrixHeight);
        return bitmap;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            AlertDialog.Builder alert = new AlertDialog.Builder(CerrarVistaActivity.this);
            alert.setTitle("Atención!")
                    .setMessage("No puedes salir sin antes leer tu codigo QR")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Log.d("Login", "salir qr");
                        }
                    })
                    .show();
        }
        return super.onKeyDown(keyCode, event);
    }

    private boolean estadoConexión() {
        boolean enabled = true;

        ConnectivityManager connectivityManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = null;
        if (connectivityManager != null) {
            info = connectivityManager.getActiveNetworkInfo();
        }

        if ((info == null || !info.isConnected() || !info.isAvailable())) {
            Toast.makeText(CerrarVistaActivity.this, "NO HAY CONEXIÓN A INTERNET ", Toast.LENGTH_LONG).show();
            enabled = false;
        }
        return enabled;
    }
}
