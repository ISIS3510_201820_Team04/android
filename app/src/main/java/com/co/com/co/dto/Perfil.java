package com.co.com.co.dto;

public final class Perfil extends PerfilDto {

    private static PerfilDto profile;

    public Perfil(){}

    public static PerfilDto getProfile() {
        return profile;
    }

    public static void setProfile(PerfilDto perfil) {
        profile = perfil;
    }

    public  static void clearPerfil(){
        profile = null;
    }

}
