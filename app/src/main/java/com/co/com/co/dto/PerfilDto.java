package com.co.com.co.dto;

import com.google.firebase.database.Exclude;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PerfilDto {

    private String nickname;
    private String email;
    private String cedula;
    private String nombre;
    private String apellido;
    private String conjunto;
    private String apto;
    private String pass;
    private HashMap<String, PerfilDto> contactos;
    private VehiculoDto vehiculo;
    public PerfilDto perfil;
    private  String tokenFireBase;
    private boolean vista;

    public PerfilDto() {

    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getConjunto() {
        return conjunto;
    }

    public void setConjunto(String conjunto) {
        this.conjunto = conjunto;
    }

    public String getApto() {
        return apto;
    }

    public void setApto(String apto) {
        this.apto = apto;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public HashMap<String, PerfilDto> getContactos() {
        return contactos;
    }

    public void setContactos(HashMap<String, PerfilDto> contactos) {
        this.contactos = contactos;
    }

    public VehiculoDto getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(VehiculoDto vehiculo) {
        this.vehiculo = vehiculo;
    }

    public PerfilDto getPerfil() {
        return perfil;
    }

    public void setPerfil(PerfilDto perfil) {
        this.perfil = perfil;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("uid", nickname);
        result.put("author", email);
        result.put("title", apto);

        return result;
    }


    public String getTokenFireBase() {
        return tokenFireBase;
    }

    public void setTokenFireBase(String tokenFireBase) {
        this.tokenFireBase = tokenFireBase;
    }

    public boolean isVista() {
        return vista;
    }

    public void setVista(boolean vista) {
        this.vista = vista;
    }

    public void clearAll(){
        this.apellido= null;
        this.apto= null;
        this.cedula= null;
        this.nickname = null;
        this.pass = null;
        this.perfil = null;
        this.nombre = null;
        this.email = null;
        this.conjunto= null;
        this.contactos = null;
        this.vehiculo = null;
        this.tokenFireBase= null;
    }


}
