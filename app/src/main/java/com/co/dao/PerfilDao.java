package com.co.dao;

import android.support.annotation.NonNull;

import com.co.com.co.dto.Perfil;
import com.co.com.co.dto.PerfilDto;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;

public class PerfilDao {
    private DatabaseReference myRef;

    public PerfilDao(String usuario) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        if(usuario.isEmpty() || usuario.equals(null)){
            myRef = database.getReference("Perfiles");
        }else{
            myRef = database.getReference("Perfiles").child(usuario);
        }

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
               // for (DataSnapshot messageSnapshot: dataSnapshot.getChildren()) {
                //   String email = (String) messageSnapshot.child("email").getValue();
                  //  String cedula = (String) messageSnapshot.child("cedula").getValue();
                  //  System.out.print("--------------------------------" + email);
               // }

               // PerfilDto perfil = dataSnapshot.getValue(PerfilDto.class);
               // Perfil.setProfile(perfil);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

    }

    public Task<Void> insertPerfil(PerfilDto perfil, String nickname) {
      return   myRef.child(nickname).setValue(perfil).addOnSuccessListener(new OnSuccessListener<Void>() {

            @Override
            public void onSuccess(Void aVoid) {
               return;
            }
        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                     return;
                    }
                });
    }

    public Task<Void> insertObject(String nickname, String child, String clave, String valor)
    {
        return myRef.child(nickname).child(child).child(valor).child(clave).setValue(valor).addOnSuccessListener(new OnSuccessListener<Void>() {

            @Override
            public void onSuccess(Void aVoid) {
                return;
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                return;
            }
        });
    }

    public Task<Void> actualizar(Object obj , String nickname)
    {
        return myRef.child(nickname).setValue(obj).addOnSuccessListener(new OnSuccessListener<Void>() {

            @Override
            public void onSuccess(Void aVoid) {
                return;
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                return;
            }
        });
    }

}
