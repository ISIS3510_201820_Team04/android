package com.co.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.co.rere.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeSet;

public class VisitasAdapter extends
        RecyclerView.Adapter<VisitasAdapter.ViewHolder> {

    public static List<String> visitas= new ArrayList<>();
    private TreeSet filtering= new TreeSet();


    public VisitasAdapter(List<String> mVisitas) {
        this.visitas.addAll(mVisitas);
        notifyDataSetChanged();
    }


    public void setList(List<String> mVisitas) {
        this.visitas.clear();
        this.visitas.addAll(mVisitas);
        notifyDataSetChanged();
    }

    public void setPlaca(String mVisitas) {
        filtering.add(mVisitas);
        this.visitas.clear();
        this.visitas.addAll(filtering);
        notifyDataSetChanged();
    }

    // Usually involves inflating a layout from XML and returning the holder
    @Override
    public VisitasAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.item_visita, parent, false);

        // Return a new holder instance
        return new ViewHolder(contactView);
    }

    // Involves populating data into the item through holder
    @Override
    public void onBindViewHolder(VisitasAdapter.ViewHolder viewHolder, int position) {
        // Get the data model based on position
        String contact = visitas.get(position);

        // Set item views based on your views and data model
        viewHolder.visitaName.setText(contact);
    }

    // Returns the total count of items in the list
    @Override
    public int getItemCount() {
        return visitas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView visitaName;


        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);

            visitaName = (TextView) itemView.findViewById(R.id.visita_name);

        }
    }
}