package com.co.adapters;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.co.fragments.CeladorContadorFragment;
import com.co.fragments.CeladorInicioFragment;
import com.co.fragments.CeladorSearchFragment;
import com.co.rere.R;

public class CeladorAdapter extends FragmentPagerAdapter {

    private Context mContext;

    public CeladorAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    // This determines the fragment for each tab
    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new CeladorInicioFragment();
        } else if (position == 1) {
            return new CeladorSearchFragment();
        } else if (position == 2) { 
            return new CeladorContadorFragment();

        } else {
            return new CeladorInicioFragment();
        }
    }

    // This determines the number of tabs
    @Override
    public int getCount() {
        return 3;
    }

    // This determines the title for each tab
    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return mContext.getString(R.string.celador_inicio);
            case 1:
                return mContext.getString(R.string.celador_search);
            case 2:
                return mContext.getString(R.string.celador_contador);

                default:
                    return mContext.getString(R.string.celador_inicio);
        }
    }


}
