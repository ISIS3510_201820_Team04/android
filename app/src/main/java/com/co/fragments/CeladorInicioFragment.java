package com.co.fragments;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.co.com.co.dto.Perfil;
import com.co.rere.LoginConjuntoActivity;
import com.co.rere.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.integration.android.IntentIntegrator;

import static com.co.rere.CeladorActivity.leido;


public class CeladorInicioFragment extends Fragment {
    private static final int CAMERA_PIC_REQUEST = 408;
    private static final int LOCATION_CODE = 893;

    private Snackbar snackbar;
    private FrameLayout parentView;


    static Context context;
    private DatabaseReference myRef;
    TextView ocupado;
    TextView libre;
    TextView nombreConjunto;


    TextView disponibleVisita;
    TextView ocupadosVisita;

    String placaDefecto;
    String usuarioPlaca;

    DatabaseReference mDatabase_solicitudes;
    DatabaseReference mDatabase_solicitudes2;
    int conteo_parqueaderos;
    int conteo_ocupados;
    int getConteo_disponibles;


    String aptoVisita;
    String horaingreso;
    String parqueoOcupado;


    public CeladorInicioFragment() {
        // Required empty public constructor
    }

    public static CeladorInicioFragment newInstance(String param1, String param2) {
        CeladorInicioFragment fragment = new CeladorInicioFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }



    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_celador, container, false);
        initAll(view);

        if(connectionState())
        {

        }
        else if (!connectionState())
        {
            //Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT);
            //toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            //toast.show();
            //showAlertInternet();
            showSnackBarInternet();
        }
        return view;
    }

    private void initAll(View view) {

        context = getActivity();
        ocupado = view.findViewById(R.id.txtOcupados);
        libre = view.findViewById(R.id.txtLibres);
        nombreConjunto = view.findViewById(R.id.txtNombreConjunto);


        disponibleVisita = view.findViewById(R.id.txtVisitantesLibres);
        ocupadosVisita = view.findViewById(R.id.txtVisitantesOcupados);

        parentView = (FrameLayout) view.findViewById(R.id.frameLayout);

        aptoVisita = "";
        horaingreso = "";
        parqueoOcupado = "";
        usuarioPlaca = "";

        placaDefecto = "ICR742";


        String conjuntoName = "Conjunto: "+LoginConjuntoActivity.conjuntoSeleccionado;
        nombreConjunto.setText(conjuntoName);


        //leer codigo QR
        view.findViewById(R.id.fab_my_location).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!connectionState()){
                   showAlertInternet();
                }
                else{

                   readQRCode();

                }
            }
        });


        /////////////////// FIREBASE   //////////////////////////

        final FirebaseDatabase database = FirebaseDatabase.getInstance();

        if (database != null) {

            mDatabase_solicitudes = database.getReference().child("Conjuntos/"+LoginConjuntoActivity.conjuntoSeleccionado+"/ParqueaderosPropietarios");
            mDatabase_solicitudes.addValueEventListener(new ValueEventListener() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    if (connectionState()) {
                        conteo_parqueaderos = 0;
                        conteo_ocupados = 0;
                        getConteo_disponibles = 0;

                        conteo_parqueaderos = (int) dataSnapshot.getChildrenCount();

                        for (DataSnapshot childSnapShot : dataSnapshot.getChildren()) {
                            if (childSnapShot != null) {
                                if (String.valueOf(childSnapShot.child("Estado").getValue()).equals("Disponible")) {
                                    getConteo_disponibles++;

                                } else if (String.valueOf(childSnapShot.child("Estado").getValue()).equals("Ocupado")) {
                                    conteo_ocupados++;

                                }
                            }

                        }

                        String disponibles = "Libres : " + getConteo_disponibles;
                        String ocupados = "Ocupados : " + conteo_ocupados;

                        libre.setText(String.valueOf(disponibles));
                        ocupado.setText(String.valueOf(ocupados));
                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(getActivity(), "Error SOLICITUDES VEH!" + databaseError.toException(), Toast.LENGTH_LONG).show();

                }

            });


            mDatabase_solicitudes2 = database.getReference().child("Conjuntos/"+LoginConjuntoActivity.conjuntoSeleccionado+"/ParqueaderosVisitantes");
            mDatabase_solicitudes2.addValueEventListener(new ValueEventListener() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    conteo_parqueaderos = 0;
                    conteo_ocupados = 0;
                    getConteo_disponibles = 0;
                    if (connectionState()) {

                        conteo_parqueaderos = (int) dataSnapshot.getChildrenCount();

                        for (DataSnapshot childSnapShot : dataSnapshot.getChildren()) {
                            if (String.valueOf(childSnapShot.child("Estado").getValue()).equals("Disponible")) {
                                getConteo_disponibles++;

                            } else if (String.valueOf(childSnapShot.child("Estado").getValue()).equals("Ocupado")) {
                                conteo_ocupados++;

                            }
                        }

                        String disponibles = "Libres : " + getConteo_disponibles;
                        String ocupados = "Ocupados : " + conteo_ocupados;
                        disponibleVisita.setText(String.valueOf(disponibles));
                        ocupadosVisita.setText(String.valueOf(ocupados));
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(getActivity(), "Error SOLICITUDES VEH!" + databaseError.toException(), Toast.LENGTH_LONG).show();
                }

            });
        }


    }


    private void showAlertInternet() {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle("Alerta");
        alertDialog.setMessage("No hay conexión a internet, verifique su conexión y vuelva a intentar");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Entendido",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        //finish();
                    }
                });
        alertDialog.show();
    }


    private void showSnackBarInternet() {

        snackbar = Snackbar
                .make(parentView, R.string.no_internet, Snackbar.LENGTH_INDEFINITE);
//                .setAction("REINTENTAR", new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                    }
//                });

// Changing message text color
        //snackbar.setActionTextColor(Color.RED);

// Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.RED);
//        FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)sbView.getLayoutParams();
//        params.gravity = Gravity.TOP;
//        sbView.setLayoutParams(params);
        snackbar.show();
    }



    private boolean connectionState() {
        boolean enabled = true;

        Activity activity = getActivity();
        if (activity != null) {
            ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo info = null;
            if (connectivityManager != null) {
                info = connectivityManager.getActiveNetworkInfo();
            }

            if ((info == null || !info.isConnected() || !info.isAvailable())) {
                //Toast toast = Toast.makeText(activity, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT);
                //toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                //toast.show();
                enabled = false;
            }
            return enabled;
        } else {
            return false;
        }
    }





    private void readQRCode(){


                IntentIntegrator integrator = new IntentIntegrator(getActivity());
                integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
                integrator.setPrompt("Scan");
                integrator.setCameraId(0);
                integrator.setBeepEnabled(false);
                integrator.setBarcodeImageEnabled(false);
                integrator.initiateScan();

    }


}