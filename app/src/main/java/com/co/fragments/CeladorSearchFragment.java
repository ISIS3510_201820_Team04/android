package com.co.fragments;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
//import com.google.android.gms.tasks.Task;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.co.adapters.VisitasAdapter;
import com.co.rere.CeladorActivity;
import com.co.rere.CeladorContador;
import com.co.rere.LoginConjuntoActivity;
import com.co.rere.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;




public class CeladorSearchFragment extends Fragment {

    private VisitasAdapter adapter;
    private ProgressBar progressBar;
    DatabaseReference mDatabase_perfiles;
    DatabaseReference mDatabase_placas;
    public static ArrayList<String> visitantes = new ArrayList<>();
    int count =0;

    int conteo_placas;

    public static ArrayList<String> listaPlacas = new ArrayList<>();

    public static String placaSeleccionada;




    private Snackbar snackbar;

    private ConstraintLayout parentView;




    public CeladorSearchFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_celador_search, container, false);
       initAll(view);
        if(connectionState())
        {

        }
        else if (!connectionState())
        {
            //Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT);
            //toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            //toast.show();
            showSnackBarInternet();
            showAlertInternet();
        }
       return view;
    }

    private void initAll(View view) {

        parentView = (ConstraintLayout) view.findViewById(R.id.frameLayout2);
        placaSeleccionada="PRUEBA";
        final ListView listview = (ListView) view.findViewById(R.id.listViewS);



        //-----------------------------------------//
        //------------ Firebase -------------------//
        //-----------------------------------------//

        //Inicio consulta perfiles
        final FirebaseDatabase database = FirebaseDatabase.getInstance();

        mDatabase_placas = database.getReference().child("Conjuntos/"+LoginConjuntoActivity.conjuntoSeleccionado+"/Visitas");
        mDatabase_placas.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot childSnapShot : dataSnapshot.getChildren()) {


                    if (String.valueOf(childSnapShot.child("IdVisitante").getValue()) != null)
                        visitantes.add(String.valueOf(childSnapShot.child("IdVisitante").getValue()));

                    //Final consulta perfiles
                }

                if (visitantes != null)
                    for (String idVisitante : visitantes) {
                        mDatabase_perfiles = database.getReference().child("Perfiles/" + idVisitante);


                        mDatabase_perfiles.addListenerForSingleValueEvent(new ValueEventListener() {
                            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot2) {

                                if (String.valueOf(dataSnapshot2.child("vehiculo").getValue()) != null) {

                                    conteo_placas = 0;
                                    conteo_placas = (int) dataSnapshot2.getChildrenCount();

                                    String valor = String.valueOf(dataSnapshot2.getValue());

                                    for (DataSnapshot vehiculoChildSnapShot : dataSnapshot2.child("vehiculo").getChildren()) {

                                        Log.v("xxx", "id-" + vehiculoChildSnapShot.getKey());

                                        if (vehiculoChildSnapShot.getKey()!=null && vehiculoChildSnapShot.getKey().equals("placa")) {
                                            listaPlacas.add(vehiculoChildSnapShot.getValue().toString());
                                        }
                                        conteo_placas++;

                                    }

                                }

                                //--------------------------------------------//
                                //------------ ListView ----------------------//
                                //--------------------------------------------//

                                final ArrayList<String> list = new ArrayList<String>();
                                for (int i = 0; i < listaPlacas.size(); ++i) {
                                    if (!list.contains(listaPlacas.get(i)))
                                        list.add(listaPlacas.get(i));
                                }

                                final ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, list);
                                listview.setAdapter(adapter);

                                // ListView Item Click Listener
                                listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view,
                                                            int position, long id) {

                                        // ListView Clicked item index
                                        int itemPosition     = position;
                                        // ListView Clicked item value
                                        String  itemValue    = (String) listview.getItemAtPosition(position);

                                        // Show Alert
                                        //   Toast.makeText(getApplicationContext(), "Position :"+itemPosition+"  ListItem : " +itemValue , Toast.LENGTH_LONG).show();


                                        placaSeleccionada = itemValue;
                                       // Toast.makeText(getActivity(), placaSeleccionada, Toast.LENGTH_LONG).show();


                                       Intent mainIntent = new Intent().setClass(getActivity(), CeladorContador.class);
                                       startActivity(mainIntent);
                                   //  getFragmentManager().beginTransaction().replace(R.id.frameLayout3, new tasks()).commit();

                                    }

                                });

                            }


                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                Toast.makeText(getActivity(), "Error SOLICITUDES VEH!" + databaseError.toException(), Toast.LENGTH_LONG).show();

                            }

                        });

                    }



            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getActivity(), "Error SOLICITUDES VEH!" + databaseError.toException(), Toast.LENGTH_LONG).show();

            }
        });

    }


    private boolean connectionState() {
        boolean enabled = true;

        Activity activity = getActivity();
        if (activity != null) {
            ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo info = null;
            if (connectivityManager != null) {
                info = connectivityManager.getActiveNetworkInfo();
            }

            if ((info == null || !info.isConnected() || !info.isAvailable())) {
                //Toast toast = Toast.makeText(activity, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT);
                //toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                //toast.show();
                enabled = false;
            }
            return enabled;
        } else {
            return false;
        }
    }

    private void showAlertInternet() {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle("Alerta");
        alertDialog.setMessage("No hay conexión a internet, verifique su conexión y vuelva a intentar");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Entendido",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        //finish();
                    }
                });
        alertDialog.show();
    }


    private void showSnackBarInternet() {

        snackbar = Snackbar
                .make(parentView, R.string.no_internet, Snackbar.LENGTH_INDEFINITE);

// Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.RED);
        snackbar.show();
    }



}