package com.co.fragments;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.co.com.co.dto.Perfil;
import com.co.com.co.dto.PerfilDto;
import com.co.rere.CeladorContador;
import com.co.rere.CeladorParqueaderos;
import com.co.rere.LoginConjuntoActivity;
import com.co.rere.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


public class CeladorContadorFragment extends Fragment {



    DatabaseReference mDatabase_parqueaderoPropietarios;
    DatabaseReference mDatabase_parqueaderoVisitantes;

    ArrayList<String> parqueaderos = new ArrayList<>();

    public static String parqueaderoSeleccionado;


    public CeladorContadorFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_celador_contador, container, false);
        initAll(view);

        if (connectionState()) {

        } else if (!connectionState()) {
            //Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT);
            //toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            //toast.show();
            showAlertInternet();
        }


        return view;
    }

    private void initAll(View view) {


        parqueaderoSeleccionado="";
        final ListView listview = (ListView) view.findViewById(R.id.listViewC);

        //-----------------------------------------------------------//
        //-------------- Firebase ----------------------------------//
        //---------------------------------------------------------//

        //-----------Inicio consulta parqueaderos Visitantes----------//
        mDatabase_parqueaderoVisitantes = FirebaseDatabase.getInstance().getReference().child("Conjuntos/" + LoginConjuntoActivity.conjuntoSeleccionado + "/ParqueaderosVisitantes");
        mDatabase_parqueaderoVisitantes.addValueEventListener(new ValueEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                for (DataSnapshot childSnapShot : dataSnapshot.getChildren()) {

                    if (childSnapShot.child("Estado") != null) {

                        parqueaderos.add(childSnapShot.getKey());
                    }

                }

                //-----------Inicio consulta parqueaderos Propietarios----------//
                mDatabase_parqueaderoPropietarios = FirebaseDatabase.getInstance().getReference().child("Conjuntos/" + LoginConjuntoActivity.conjuntoSeleccionado + "/ParqueaderosPropietarios");
                mDatabase_parqueaderoPropietarios.addValueEventListener(new ValueEventListener() {
                    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot2) {


                        for (DataSnapshot childSnapShot : dataSnapshot2.getChildren()) {

                            if (childSnapShot.child("Estado") != null) {

                                parqueaderos.add(childSnapShot.getKey());
                            }

                        }


                        //--------------------------------------------//
                        //------------ ListView ----------------------//
                        //--------------------------------------------//

                        final ArrayList<String> list = new ArrayList<String>();
                        for (int i = 0; i < parqueaderos.size(); ++i) {
                            if (!list.contains(parqueaderos.get(i)))
                                list.add(parqueaderos.get(i));
                        }

                        final ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, list);
                        listview.setAdapter(adapter);

                        // ListView Item Click Listener
                        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                            @Override
                            public void onItemClick(AdapterView<?> parent, View view,
                                                    int position, long id) {

                                // ListView Clicked item index
                                int itemPosition     = position;
                                // ListView Clicked item value
                                String  itemValue    = (String) listview.getItemAtPosition(position);

                                // Show Alert
                                //   Toast.makeText(getApplicationContext(), "Position :"+itemPosition+"  ListItem : " +itemValue , Toast.LENGTH_LONG).show();


                                parqueaderoSeleccionado = itemValue;


                                Intent mainIntent = new Intent().setClass(getActivity(), CeladorParqueaderos.class);
                                startActivity(mainIntent);

                            }

                        });


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Toast.makeText(getActivity(), "Cancelado " + databaseError.toException(), Toast.LENGTH_LONG).show();

                    }

                });




            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getActivity(), "Cancelado " + databaseError.toException(), Toast.LENGTH_LONG).show();

            }

        });



    }


    private boolean connectionState() {
        boolean enabled = true;

        Activity activity = getActivity();
        if (activity != null) {
            ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo info = null;
            if (connectivityManager != null) {
                info = connectivityManager.getActiveNetworkInfo();
            }

            if ((info == null || !info.isConnected() || !info.isAvailable())) {
                //  Toast toast = Toast.makeText(activity, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT);
                // toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                //toast.show();
                enabled = false;
            }
            return enabled;
        } else {
            return false;
        }
    }

    private void showAlertInternet() {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle("Alerta");
        alertDialog.setMessage("No hay conexión a internet, verifique su conexión y vuelva a intentar");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Entendido",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        //finish();
                    }
                });
        alertDialog.show();
    }



}
